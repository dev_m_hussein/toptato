package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Size {

    @SerializedName(value = "sizeId", alternate = {"SizeId", "Id"})
    @Expose
    private Integer sizeId;
    @SerializedName(value = "sizeArabicName", alternate = {"ItemSizeArabicName", "SizeArabicName", "ArabicName"})
    @Expose
    private String sizeArabicName;
    @SerializedName(value = "sizeEnglishName", alternate = {"ItemSizeEnglishName", "SizeEnglishName", "EnglishName"})
    @Expose
    private String sizeEnglishName;
    @SerializedName("colors")
    @Expose
    private List<Color> colors = null;

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getSizeArabicName() : getSizeEnglishName();
    }

    public String getSizeArabicName() {
        return sizeArabicName;
    }

    public void setSizeArabicName(String sizeArabicName) {
        this.sizeArabicName = sizeArabicName;
    }

    public String getSizeEnglishName() {
        return sizeEnglishName;
    }

    public void setSizeEnglishName(String sizeEnglishName) {
        this.sizeEnglishName = sizeEnglishName;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }
}
