package com.finalproject.toptato.model.utils.validation;

/**
 * Created by mahmoud saad
 */
public class PhoneNumberValid extends EmptyValid {
    @Override
    public boolean valid(String str) {
        return super.valid(str)&& str.matches("^+[0-9]{11}$");
    }
}
