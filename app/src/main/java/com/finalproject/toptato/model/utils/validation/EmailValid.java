package com.finalproject.toptato.model.utils.validation;

import android.util.Patterns;

/**
 * Created by mahmoud saad
 */
public class EmailValid extends EmptyValid {
    @Override
    public boolean valid(String str) {
        return super.valid(str)&&Patterns.EMAIL_ADDRESS.matcher(str).matches();
    }
}
