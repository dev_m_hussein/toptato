package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.SerializedName;

public class ResendActivation {

    @SerializedName("arabicMessage")
    private String arabicMessage;

    @SerializedName("success")
    private String success;

    @SerializedName("englishMessage")
    private String englishMessage;

    @SerializedName("error")
    private String error;

    public String getMessage(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicMessage() : getEnglishMessage();
    }

    public String getArabicMessage() {
        return arabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        this.arabicMessage = arabicMessage;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getEnglishMessage() {
        return englishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        this.englishMessage = englishMessage;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return
                "ResendActivation{" +
                        "arabicMessage = '" + arabicMessage + '\'' +
                        ",success = '" + success + '\'' +
                        ",englishMessage = '" + englishMessage + '\'' +
                        ",error = '" + error + '\'' +
                        "}";
    }
}