package com.finalproject.toptato.model.entities.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayTabsInfo {

    @SerializedName("MerchantEmail")
    @Expose
    private String merchantEmail;
    @SerializedName("SecretKey")
    @Expose
    private String secretKey;

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

}