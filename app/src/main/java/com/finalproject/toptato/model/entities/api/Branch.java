package com.finalproject.toptato.model.entities.api;

import android.content.Context;
import android.text.TextUtils;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Branch implements Serializable {

    @SerializedName("CountryId")
    private int countryId;

    @SerializedName("Address")
    private String address;

    @SerializedName("BreifAR")
    private String breifAR;

    @SerializedName("Y_Piont")
    private String yPiont;

    @SerializedName("X_Piont")
    private String xPiont;

    @SerializedName("Tele")
    private String tele;

    @SerializedName("ArabicName")
    private String arabicName;

    @SerializedName("CountryArabicName")
    private String countryArabicName;

    @SerializedName("Id")
    private int id;

    @SerializedName("Fax")
    private String fax;

    @SerializedName("CountryEnglishName")
    private String countryEnglishName;

    @SerializedName("EnglishName")
    private String englishName;

    @SerializedName("BreifEN")
    private String breifEN;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBreifAR() {
        return breifAR;
    }

    public void setBreifAR(String breifAR) {
        this.breifAR = breifAR;
    }

    public String getYPiont() {
        return yPiont;
    }

    public void setYPiont(String yPiont) {
        this.yPiont = yPiont;
    }

    public String getXPiont() {
        return xPiont;
    }

    public void setXPiont(String xPiont) {
        this.xPiont = xPiont;
    }

    public String getTele() {
        return tele;
    }

    public void setTele(String tele) {
        this.tele = tele;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getCountryArabicName() {
        return countryArabicName;
    }

    public void setCountryArabicName(String countryArabicName) {
        this.countryArabicName = countryArabicName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCountryEnglishName() {
        return countryEnglishName;
    }

    public void setCountryEnglishName(String countryEnglishName) {
        this.countryEnglishName = countryEnglishName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getBreifEN() {
        return breifEN;
    }

    public void setBreifEN(String breifEN) {
        this.breifEN = breifEN;
    }

    public String getName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicName() : getEnglishName();
    }

    public String getBreif(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getBreifAR() : getBreifEN();
    }

    public String getCountryName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getCountryArabicName() : getCountryEnglishName();
    }

    public LatLng getLocation() {
        if (TextUtils.isEmpty(getXPiont()) || TextUtils.isEmpty(getYPiont())) return null;
        return new LatLng(Double.parseDouble(getXPiont()), Double.parseDouble(getYPiont()));
    }
}