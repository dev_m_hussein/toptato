package com.finalproject.toptato.model.entities.api.cartitemdetails;

import com.google.gson.annotations.SerializedName;

public class TBLOrderItemsItem{

	@SerializedName("ItemStatus")
	private int itemStatus;

	@SerializedName("ItemNote")
	private String itemNote;

	@SerializedName("TotalValue")
	private double totalValue;

	@SerializedName("UnitPrice")
	private double unitPrice;

	@SerializedName("ItemQTY")
	private double itemQTY;

	@SerializedName("IsPromotion")
	private boolean isPromotion;

	@SerializedName("ItemId")
	private int itemId;

	@SerializedName("ItemRefNumber")
	private String itemRefNumber;

	@SerializedName("SizeId")
	private int sizeId;

	@SerializedName("ColorId")
	private int colorId;

	public void setItemStatus(int itemStatus){
		this.itemStatus = itemStatus;
	}

	public int getItemStatus(){
		return itemStatus;
	}

	public void setItemNote(String itemNote){
		this.itemNote = itemNote;
	}

	public String getItemNote(){
		return itemNote;
	}

	public void setTotalValue(double totalValue){
		this.totalValue = totalValue;
	}

	public double getTotalValue(){
		return totalValue;
	}

	public void setUnitPrice(double unitPrice){
		this.unitPrice = unitPrice;
	}

	public double getUnitPrice(){
		return unitPrice;
	}

	public void setItemQTY(double itemQTY){
		this.itemQTY = itemQTY;
	}

	public double getItemQTY(){
		return itemQTY;
	}

	public void setIsPromotion(boolean isPromotion){
		this.isPromotion = isPromotion;
	}

	public boolean isIsPromotion(){
		return isPromotion;
	}

	public void setItemId(int itemId){
		this.itemId = itemId;
	}

	public int getItemId(){
		return itemId;
	}

	public void setItemRefNumber(String itemRefNumber){
		this.itemRefNumber = itemRefNumber;
	}

	public String getItemRefNumber(){
		return itemRefNumber;
	}

	public void setSizeId(int sizeId){
		this.sizeId = sizeId;
	}

	public int getSizeId(){
		return sizeId;
	}

	public void setColorId(int colorId){
		this.colorId = colorId;
	}

	public int getColorId(){
		return colorId;
	}

	@Override
 	public String toString(){
		return 
			"TBLOrderItemsItem{" + 
			"itemStatus = '" + itemStatus + '\'' + 
			",itemNote = '" + itemNote + '\'' + 
			",totalValue = '" + totalValue + '\'' + 
			",unitPrice = '" + unitPrice + '\'' + 
			",itemQTY = '" + itemQTY + '\'' + 
			",isPromotion = '" + isPromotion + '\'' + 
			",itemId = '" + itemId + '\'' + 
			",itemRefNumber = '" + itemRefNumber + '\'' + 
			",sizeId = '" + sizeId + '\'' + 
			",colorId = '" + colorId + '\'' + 
			"}";
		}
}