package com.finalproject.toptato.model.utils.customViews.colorSelectorView.anim;

/**
 * Created by Osman Ibrahiem
 */
public class AnimatedInteger extends AnimatedValue<Integer> {

    public AnimatedInteger(int value) {
        super(value);
    }

    /**
     * Get the next value about to be drawn, without setting
     * the current value to it.
     *
     * @param start    The time at which the animation started,
     *                 in milliseconds.
     * @param duration The duration, in milliseconds, that
     *                 the animation should take.
     * @return The next value.
     */
    @Override
    public Integer nextVal(long start, long duration) {
        int difference = (int) ((getTarget() - val()) * Math.sqrt((double) (System.currentTimeMillis() - start) / (duration)));
        if (Math.abs(getTarget() - val()) > 1 && System.currentTimeMillis() - start < duration)
            return val() + (getTarget() < val() ? Math.min(difference, -1) : Math.max(difference, 1));
        else return getTarget();
    }

}
