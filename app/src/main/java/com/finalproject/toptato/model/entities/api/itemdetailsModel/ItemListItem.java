package com.finalproject.toptato.model.entities.api.itemdetailsModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemListItem{

	@SerializedName("CategoryId")
	private int categoryId;

	@SerializedName("ItemCode")
	private String itemCode;

	@SerializedName("CategoryImage")
	private String categoryImage;

	@SerializedName("ReviewStars")
	private int reviewStars;

	@SerializedName("IsActive")
	private boolean isActive;

	@SerializedName("ArabicDetails")
	private String arabicDetails;

	@SerializedName("BrandId")
	private int brandId;

	@SerializedName("ImagesList")
	private List<ImagesListItem> imagesList;

	@SerializedName("DescEN")
	private String descEN;

	@SerializedName("CategoryArabicName")
	private String categoryArabicName;

	@SerializedName("PromotionUnitPrice")
	private int promotionUnitPrice;

	@SerializedName("EnglishDetails")
	private Object englishDetails;

	@SerializedName("BrandArabicName")
	private String brandArabicName;

	@SerializedName("MatrixList")
	private List<MatrixListItem> matrixList;

	@SerializedName("RefNumber")
	private String refNumber;

	@SerializedName("DescAR")
	private Object descAR;

	@SerializedName("RecieveDetails")
	private List<Object> recieveDetails;

	@SerializedName("ArabicName")
	private String arabicName;

	@SerializedName("UnitPrice")
	private int unitPrice;

	@SerializedName("CategoryVideo")
	private String categoryVideo;

	@SerializedName("IsNewItem")
	private boolean isNewItem;

	@SerializedName("CategoryEnglishName")
	private String categoryEnglishName;

	@SerializedName("IsPromotion")
	private boolean isPromotion;

	@SerializedName("Id")
	private int id;

	@SerializedName("BrandEnglishName")
	private String brandEnglishName;

	@SerializedName("EnglishName")
	private String englishName;

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setItemCode(String itemCode){
		this.itemCode = itemCode;
	}

	public String getItemCode(){
		return itemCode;
	}

	public void setCategoryImage(String categoryImage){
		this.categoryImage = categoryImage;
	}

	public String getCategoryImage(){
		return categoryImage;
	}

	public void setReviewStars(int reviewStars){
		this.reviewStars = reviewStars;
	}

	public int getReviewStars(){
		return reviewStars;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setArabicDetails(String arabicDetails){
		this.arabicDetails = arabicDetails;
	}

	public String getArabicDetails(){
		return arabicDetails;
	}

	public void setBrandId(int brandId){
		this.brandId = brandId;
	}

	public int getBrandId(){
		return brandId;
	}

	public void setImagesList(List<ImagesListItem> imagesList){
		this.imagesList = imagesList;
	}

	public List<ImagesListItem> getImagesList(){
		return imagesList;
	}

	public void setDescEN(String descEN){
		this.descEN = descEN;
	}

	public String getDescEN(){
		return descEN;
	}

	public void setCategoryArabicName(String categoryArabicName){
		this.categoryArabicName = categoryArabicName;
	}

	public String getCategoryArabicName(){
		return categoryArabicName;
	}

	public void setPromotionUnitPrice(int promotionUnitPrice){
		this.promotionUnitPrice = promotionUnitPrice;
	}

	public int getPromotionUnitPrice(){
		return promotionUnitPrice;
	}

	public void setEnglishDetails(Object englishDetails){
		this.englishDetails = englishDetails;
	}

	public Object getEnglishDetails(){
		return englishDetails;
	}

	public void setBrandArabicName(String brandArabicName){
		this.brandArabicName = brandArabicName;
	}

	public String getBrandArabicName(){
		return brandArabicName;
	}

	public void setMatrixList(List<MatrixListItem> matrixList){
		this.matrixList = matrixList;
	}

	public List<MatrixListItem> getMatrixList(){
		return matrixList;
	}

	public void setRefNumber(String refNumber){
		this.refNumber = refNumber;
	}

	public String getRefNumber(){
		return refNumber;
	}

	public void setDescAR(Object descAR){
		this.descAR = descAR;
	}

	public Object getDescAR(){
		return descAR;
	}

	public void setRecieveDetails(List<Object> recieveDetails){
		this.recieveDetails = recieveDetails;
	}

	public List<Object> getRecieveDetails(){
		return recieveDetails;
	}

	public void setArabicName(String arabicName){
		this.arabicName = arabicName;
	}

	public String getArabicName(){
		return arabicName;
	}

	public void setUnitPrice(int unitPrice){
		this.unitPrice = unitPrice;
	}

	public int getUnitPrice(){
		return unitPrice;
	}

	public void setCategoryVideo(String categoryVideo){
		this.categoryVideo = categoryVideo;
	}

	public String getCategoryVideo(){
		return categoryVideo;
	}

	public void setIsNewItem(boolean isNewItem){
		this.isNewItem = isNewItem;
	}

	public boolean isIsNewItem(){
		return isNewItem;
	}

	public void setCategoryEnglishName(String categoryEnglishName){
		this.categoryEnglishName = categoryEnglishName;
	}

	public String getCategoryEnglishName(){
		return categoryEnglishName;
	}

	public void setIsPromotion(boolean isPromotion){
		this.isPromotion = isPromotion;
	}

	public boolean isIsPromotion(){
		return isPromotion;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setBrandEnglishName(String brandEnglishName){
		this.brandEnglishName = brandEnglishName;
	}

	public String getBrandEnglishName(){
		return brandEnglishName;
	}

	public void setEnglishName(String englishName){
		this.englishName = englishName;
	}

	public String getEnglishName(){
		return englishName;
	}

	@Override
 	public String toString(){
		return 
			"ItemListItem{" + 
			"categoryId = '" + categoryId + '\'' + 
			",itemCode = '" + itemCode + '\'' + 
			",categoryImage = '" + categoryImage + '\'' + 
			",reviewStars = '" + reviewStars + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",arabicDetails = '" + arabicDetails + '\'' + 
			",brandId = '" + brandId + '\'' + 
			",imagesList = '" + imagesList + '\'' + 
			",descEN = '" + descEN + '\'' + 
			",categoryArabicName = '" + categoryArabicName + '\'' + 
			",promotionUnitPrice = '" + promotionUnitPrice + '\'' + 
			",englishDetails = '" + englishDetails + '\'' + 
			",brandArabicName = '" + brandArabicName + '\'' + 
			",matrixList = '" + matrixList + '\'' + 
			",refNumber = '" + refNumber + '\'' + 
			",descAR = '" + descAR + '\'' + 
			",recieveDetails = '" + recieveDetails + '\'' + 
			",arabicName = '" + arabicName + '\'' + 
			",unitPrice = '" + unitPrice + '\'' + 
			",categoryVideo = '" + categoryVideo + '\'' + 
			",isNewItem = '" + isNewItem + '\'' + 
			",categoryEnglishName = '" + categoryEnglishName + '\'' + 
			",isPromotion = '" + isPromotion + '\'' + 
			",id = '" + id + '\'' + 
			",brandEnglishName = '" + brandEnglishName + '\'' + 
			",englishName = '" + englishName + '\'' + 
			"}";
		}
}