package com.finalproject.toptato.model.viewmodels.login;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.LoginResponse;
import com.finalproject.toptato.model.entities.api.ResendActivation;
import com.finalproject.toptato.model.utils.SingleLiveEvent;
import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.validation.EmailValid;
import com.finalproject.toptato.model.utils.validation.EmptyValid;
import com.finalproject.toptato.model.utils.validation.PasswordValid;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by mahmoud saad
 */

public class LoginViewModel extends BaseViewModel {
    public final String TAG = getClass().getSimpleName();


    private MutableLiveData<String> email = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<String> activationCode = new MutableLiveData<>();
    private MutableLiveData<String> displayMessage = new MutableLiveData<>();

    private MutableLiveData<Boolean> firstLogin = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> rememberMe = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> startNewActivity = new SingleLiveEvent<>();


    public LoginViewModel(@NonNull Application application) {
        super(application);
    }


    public MutableLiveData<String> getPassword() {
        return password;
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public MutableLiveData<String> getActivationCode() {
        return activationCode;
    }

    public MutableLiveData<Boolean> getFirstLogin() {
        return firstLogin;
    }

    public MutableLiveData<String> getDisplayMessage() {
        return displayMessage;
    }

    public MutableLiveData<Boolean> getStartNewActivity() {
        return startNewActivity;
    }

    public MutableLiveData<Boolean> getRememberMe() {
        return rememberMe;
    }

    public boolean isFirstLogin() {
        return firstLogin.getValue() != null && firstLogin.getValue();
    }

    public boolean isInputValid() {
        EmptyValid emptyValid=new EmptyValid();
        if (!emptyValid.valid(password.getValue())) {
            password.setValue("");
        }

        if (isFirstLogin() && activationCode.getValue() != null && !emptyValid.valid(activationCode.getValue())) {
            activationCode.setValue("");
            return false;
        }

        return isEmailValid() && new PasswordValid().valid(password.getValue());
    }

    public boolean isEmailValid() {
        EmptyValid emptyValid = new EmptyValid();
        if (!emptyValid.valid(email.getValue())) {
            email.setValue("");
        }
        return new EmailValid().valid(email.getValue());
    }



    public void onButtonLoginClick() {
        setIsLoading(true);

        if (!isInputValid()) {
            setIsLoading(false);
            return;
        }

        Map<String, Object> queryMap = new HashMap<>();

        queryMap.put("username", email.getValue());
        queryMap.put("password", password.getValue());

        if (isFirstLogin()) {
            queryMap.put("ActivationCode", activationCode.getValue());
        }

        getRepository()
                .userLogin(queryMap)
                .subscribe(new Observer<LoginResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "onSubscribe: ");
                    }

                    @Override
                    public void onNext(LoginResponse login) {
                        actionLogin(login);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                        Log.i(TAG, "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        setIsLoading(false);
                        Log.i(TAG, "onComplete: ");
                    }
                });

    }

    private void actionLogin(LoginResponse login) {
        setIsLoading(false);
        if (login == null) return;

        boolean isEnglish = getUserData().getLocalization() == Localization.ENGLISH_VALUE;

        if (login.getError() != null) {
            displayMessage.setValue(login.getError());
        } else if (login.getSuccess() != null) {
            switch (login.getSuccess()) {
                case "error":

                    displayMessage.setValue(getApplication().getString(R.string.message_login_email_incorrect));

                    break;

                case "ok":
                    getUserData().saveUserLogin(login);
                    startNewActivity.setValue(true);
                    break;

                case "notvalidekey":
                    if (isEnglish)
                        displayMessage.setValue(login.getEnglishMessage());
                    else
                        displayMessage.setValue(login.getArabicMessage());
                    break;

                default:
                    displayMessage.setValue("try in other time");
                    break;
            }
        }
    }


    public void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            Log.i(TAG, "handleSignInResult: success");
            Log.i(TAG, "handleSignInResult: success" + account.getDisplayName());
            Log.i(TAG, "handleSignInResult: success" + account.getEmail());
            Log.i(TAG, "handleSignInResult: success" + account.getPhotoUrl());

            Toast.makeText(getApplication(), "helll " + account.getDisplayName(), Toast.LENGTH_SHORT).show();
        } catch (ApiException e) {

            Log.i(TAG, "handleSignInResult: " + e.toString());

            Toast.makeText(getApplication(), "fail", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * check if have intent to determine it first login for user or not
     *
     * @param intent have data if is first login
     */
    public void receiveIntent(Intent intent) {
        firstLogin.setValue(false);

        if (intent == null) return;

        boolean isActivation = intent.getBooleanExtra(getApplication().getResources().getString(R.string.login_activation_code_key), false);
        if (isActivation) {
            firstLogin.setValue(true);
        }

    }

    public void onClickResendActivationcode(View view) {
        setIsLoading(true);

        if (!isEmailValid()) {
            setIsLoading(false);
            return;
        }

        Map<String, Object> queryMap = new HashMap<>();

        queryMap.put("Mail", email.getValue());


        getRepository()
                .resendActivation(queryMap)
                .subscribe(new Observer<ResendActivation>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResendActivation resendActivation) {
                        acttionResendActivation(resendActivation);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void acttionResendActivation(ResendActivation resendActivation) {
        setIsLoading(false);
        if (resendActivation == null) return;


        if (resendActivation.getError() != null) {
            displayMessage.setValue(resendActivation.getError());
        } else if (resendActivation.getSuccess() != null) {
            displayMessage.setValue(resendActivation.getMessage(getApplication()));
        } else {
            displayMessage.setValue("try in other time");
        }
    }


}
