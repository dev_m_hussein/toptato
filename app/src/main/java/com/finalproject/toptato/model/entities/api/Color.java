package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Color {

    @SerializedName(value = "colorId", alternate = {"ColorId", "Id"})
    @Expose
    private Integer colorId;
    @SerializedName("AvailableQuantity")
    @Expose
    private Integer availableQuantity;
    @SerializedName(value = "arabicName", alternate = {"ArabicName", "ColorArabicName"})
    @Expose
    private String arabicName;
    @SerializedName(value = "englishName", alternate = {"EnglishName", "ColorEnglishName"})
    @Expose
    private String englishName;
    @SerializedName(value = "code", alternate = {"Code", "ColorCode"})
    @Expose
    private String code;

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public String getName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicName() : getEnglishName();
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
