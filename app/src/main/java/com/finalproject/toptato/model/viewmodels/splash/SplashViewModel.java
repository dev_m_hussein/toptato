package com.finalproject.toptato.model.viewmodels.splash;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.model.entities.api.Token;
import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SplashViewModel extends BaseViewModel {

    private MutableLiveData<String> tokenLiveData = new MutableLiveData<>();

    public SplashViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<String> getTokenLiveData() {
        return tokenLiveData;
    }

    public void getToken() {
        getRepository()
                .getToken()
                .subscribe(new Observer<Token>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onNext(Token token) {
                        if (token != null) {
                            if (TextUtils.isEmpty(token.getError())) {
                                getUserData().saveUserInsertToken(token.getAccessToken());
                                tokenLiveData.postValue(token.getAccessToken());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        tokenLiveData.postValue(null);
                        setIsLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        setIsLoading(false);
                    }
                });

    }

    public void setLanguage(AppCompatActivity activity) {
        /*
          setting default app language Arabic
          Osman Ibrahiem
         */

        if (getUserData().getLocalization() == -1) { //-1 -> no found lang before .. first set up application
            getUserData().saveLocalization(Localization.ENGLISH_VALUE);
        }
        Localization.setLanguage(activity, getUserData().getLocalization());
    }
}
