package com.finalproject.toptato.model.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;

import com.squareup.picasso.Picasso;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;


/**
 * Created by Osman Ibrahiem
 */
public class DataBindingAdapter {

    @BindingAdapter("android:src")
    public static void setImageUri(AppCompatImageView view, String imageUri) {
        if (TextUtils.isEmpty(imageUri)) {
            view.setImageURI(null);
        } else {
            view.setImageURI(Uri.parse(imageUri));
        }
    }

    @BindingAdapter("android:src")
    public static void setImageUri(AppCompatImageView view, Uri imageUri) {
        view.setImageURI(imageUri);
    }

    @BindingAdapter("android:src")
    public static void setImageDrawable(AppCompatImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    @BindingAdapter("android:src")
    public static void setImageResource(AppCompatImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    @BindingAdapter("android:url")
    public static void setImageUrl(AppCompatImageView imageView, String url) {
        if (TextUtils.isEmpty(url) || url ==null){
            imageView.setImageResource(android.R.drawable.btn_plus);
        }
       Picasso.get().load(url).into(imageView);
    }
    }

