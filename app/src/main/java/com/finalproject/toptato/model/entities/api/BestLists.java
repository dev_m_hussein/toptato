package com.finalproject.toptato.model.entities.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BestLists {

    @SerializedName("BestSellerList")
    @Expose
    private List<Product> bestSellerList = null;
    @SerializedName("BestOfferList")
    @Expose
    private List<Product> bestOfferList = null;

    public List<Product> getBestSellerList() {
        return bestSellerList;
    }

    public void setBestSellerList(List<Product> bestSellerList) {
        this.bestSellerList = bestSellerList;
    }

    public List<Product> getBestOfferList() {
        return bestOfferList;
    }

    public void setBestOfferList(List<Product> bestOfferList) {
        this.bestOfferList = bestOfferList;
    }
}
