package com.finalproject.toptato.model.entities.api.itemdetailsModel;

import com.google.gson.annotations.SerializedName;

public class ImagesListItem{

	@SerializedName("ImagePath")
	private String imagePath;

	@SerializedName("ColorId")
	private int colorId;

	public void setImagePath(String imagePath){
		this.imagePath = imagePath;
	}

	public String getImagePath(){
		return imagePath;
	}

	public void setColorId(int colorId){
		this.colorId = colorId;
	}

	public int getColorId(){
		return colorId;
	}

	@Override
 	public String toString(){
		return 
			"ImagesListItem{" + 
			"imagePath = '" + imagePath + '\'' + 
			",colorId = '" + colorId + '\'' + 
			"}";
		}
}