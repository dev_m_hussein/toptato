package com.finalproject.toptato.model.utils.validation;

import android.os.Handler;
import android.widget.EditText;

/**
 * Created by mahmoud saad
 */
public class MethodsValid {
    private static Runnable runnable;
    public static void inputValid(EditText editText, EmptyValid valid, String textChange, Handler handler, String message, int delay){

        handler.removeCallbacks(runnable);

        runnable = () -> {
            if (!valid.valid(textChange)) {
                editText.setError(message);

            } else {
                editText.setError(null);

            }
        };
        handler.postDelayed(runnable, delay);
    }
}
