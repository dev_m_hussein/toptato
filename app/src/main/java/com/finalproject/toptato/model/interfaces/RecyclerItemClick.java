package com.finalproject.toptato.model.interfaces;

import com.finalproject.toptato.model.entities.api.Branch;

public interface RecyclerItemClick {
    void onClick(Branch position);
}
