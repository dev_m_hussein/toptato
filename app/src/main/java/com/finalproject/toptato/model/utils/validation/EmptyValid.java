package com.finalproject.toptato.model.utils.validation;

import android.text.TextUtils;

/**
 * Created by mahmoud saad
 */
public  class EmptyValid {

    public boolean valid(String str){
        if (str == null) return false;
        return !TextUtils.isEmpty(str);
    }
}
