package com.finalproject.toptato.model.viewmodels.categories;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.model.entities.api.Category;
import com.finalproject.toptato.model.entities.api.Color;
import com.finalproject.toptato.model.entities.api.MessageResponse;
import com.finalproject.toptato.model.entities.api.Product;
import com.finalproject.toptato.model.entities.api.Size;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;
import com.finalproject.toptato.repository.retrofit.ApiConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Osman Ibrahiem
 */
public class ProductsViewModel extends BaseViewModel {

    private MutableLiveData<List<Category>> categoriesList;
    private MutableLiveData<List<Color>> colorsList;
    private MutableLiveData<List<Size>> sizesList;
    private MutableLiveData<List<Product>> productsList;

    private MutableLiveData<Color> selectedFilterColor;
    private MutableLiveData<Size> selectedFilterSize;
    private int selectedCategoryID = -1;

    public ProductsViewModel(@NonNull Application application) {
        super(application);

        categoriesList = new MutableLiveData<>();
        colorsList = new MutableLiveData<>();
        sizesList = new MutableLiveData<>();
        productsList = new MutableLiveData<>();

        selectedFilterColor = new MutableLiveData<>();
        selectedFilterSize = new MutableLiveData<>();
    }

    public void setSelectedCategoryID(int selectedCategoryID) {
        this.selectedCategoryID = selectedCategoryID;
    }

    public LiveData<List<Category>> getCategoriesList() {
        getRepository()
                .getCategories()
                .subscribe(new Observer<MessageResponse<Category>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onNext(MessageResponse<Category> response) {
                        if (response.isSuccess() && response.getDataList() != null)
                            categoriesList.postValue(response.getDataList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        setIsLoading(false);
                    }
                });
        return categoriesList;
    }

    public LiveData<List<Color>> getColors() {
        getRepository()
                .getColors()
                .subscribe(new Observer<MessageResponse<Color>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(MessageResponse<Color> response) {
                        if (response.isSuccess() && response.getDataList() != null)
                            colorsList.postValue(response.getDataList());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return colorsList;
    }


    public LiveData<List<Size>> getSize() {

        getRepository()
                .getSize()
                .subscribe(new Observer<MessageResponse<Size>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(MessageResponse<Size> response) {
                        if (response.isSuccess() && response.getDataList() != null)
                            sizesList.postValue(response.getDataList());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return sizesList;
    }

    public LiveData<List<Product>> getProducts() {
        Map<String, Object> queryMap = new HashMap<>();
        if (selectedFilterColor.getValue() != null)
            queryMap.put(ApiConstants.FILTER_PARAM_COLOR_ID, selectedFilterColor.getValue().getColorId());
        if (selectedCategoryID != -1)
            queryMap.put(ApiConstants.FILTER_PARAM_CATEGORY_ID, selectedCategoryID);
        if (selectedFilterSize.getValue() != null)
            queryMap.put(ApiConstants.FILTER_PARAM_SIZE_ID, selectedFilterSize.getValue().getSizeId());

        getRepository()
                .filterProducts(queryMap)
                .subscribe(new Observer<MessageResponse<Product>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onNext(MessageResponse<Product> response) {
                        if (response.isSuccess() && response.getDataList() != null)
                            productsList.postValue(response.getDataList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        setIsLoading(false);
                    }
                });
        return productsList;
    }

    public void onColorPicked(Color color) {
        Log.wtf("mColor", "onColorPicked" + color.getCode());
        selectedFilterColor.postValue(color);
    }

    public LiveData<Color> getSelectedFilterColor() {
        return selectedFilterColor;
    }

    public LiveData<Size> getSelectedFilterSize() {
        return selectedFilterSize;
    }

}
