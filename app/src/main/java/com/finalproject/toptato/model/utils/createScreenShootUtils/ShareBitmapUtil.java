package com.finalproject.toptato.model.utils.createScreenShootUtils;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Observable;

/**
 * Created by mahmoud saad
 */

public class ShareBitmapUtil {

    public static Observable createFileDirObservable(Context context, Bitmap bitmap) {
        return Observable.defer(new Callable<Observable<File>>() {
            @Override
            public Observable<File> call() throws Exception {
                File fileDir = new File(context.getCacheDir(), "imagess");
                fileDir.mkdir();
                File file = new File(fileDir, "manosaaf.jpg");
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    return Observable.just(file);


                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

    }
}
