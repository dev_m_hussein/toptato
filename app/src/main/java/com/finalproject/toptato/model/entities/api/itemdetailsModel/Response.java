package com.finalproject.toptato.model.entities.api.itemdetailsModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response{

	@SerializedName("success")
	private String success;

	@SerializedName("ItemList")
	private List<ItemListItem> itemList;


	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setItemList(List<ItemListItem> itemList){
		this.itemList = itemList;
	}

	public List<ItemListItem> getItemList(){
		return itemList;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"success = '" + success + '\'' + 
			",itemList = '" + itemList + '\'' + 
			"}";
		}
}