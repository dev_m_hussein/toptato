package com.finalproject.toptato.model.utils.sharedTool;

import android.content.Context;
import android.content.ContextWrapper;

import com.finalproject.toptato.model.entities.api.LoginResponse;

/**
 * Created by Osman Ibrahiem
 */
public class UserData extends ContextWrapper {

    public static String TAG_LOCALIZATION = "_localization";
    public static String TAG_INSERT_TOKEN = "_insert_token";
    public static String TAG_LOGIN_DATA = "_login_data";

    private static UserData instance;

    private UserData(Context context) {
        super(context);
    }

    public static UserData getInstance(Context context) {
        if (instance == null)
            instance = new UserData(context);
        return instance;
    }

    public void saveLocalization(int value) {
        SharedPreferencesTool.setInt(this, TAG_LOCALIZATION, value);
    }

    public int getLocalization() {
        return SharedPreferencesTool.getInt(this, TAG_LOCALIZATION);
    }

    public String getLocalizationString() {
        if (getLocalization() == Localization.ARABIC_VALUE)
            return "ar";
        else return "en";
    }

    public void saveUserInsertToken(String token) {
        SharedPreferencesTool.setString(this, TAG_INSERT_TOKEN, token);
    }

    public String getUserInsertToken() {
        return SharedPreferencesTool.getString(this, TAG_INSERT_TOKEN);
    }

    public void saveUserLogin(LoginResponse login) {
        SharedPreferencesTool.saveObject(this, TAG_LOGIN_DATA, login);
    }

    public LoginResponse getUserLogin() {
        return SharedPreferencesTool.getObject(this, TAG_INSERT_TOKEN, LoginResponse.class);
    }

    public void clearShared() {
        SharedPreferencesTool.clearObject(this);
    }
}
