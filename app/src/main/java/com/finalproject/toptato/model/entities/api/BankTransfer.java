package com.finalproject.toptato.model.entities.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankTransfer {

    @SerializedName("ToAccountNumber")
    @Expose
    private String toAccountNumber;
    @SerializedName("ToAccountOwnerName")
    @Expose
    private String toAccountOwnerName;
    @SerializedName("ToBankName")
    @Expose
    private String toBankName;
    @SerializedName("TransferDate")
    @Expose
    private String transferDate;
    @SerializedName("TransferedAmount")
    @Expose
    private Integer transferedAmount;
    @SerializedName("TransferImage")
    @Expose
    private Object transferImage;
    @SerializedName("TransferNumber")
    @Expose
    private String transferNumber;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("FromBankName")
    @Expose
    private String fromBankName;
    @SerializedName("FromAccountNumber")
    @Expose
    private String fromAccountNumber;
    @SerializedName("FromAccountOwnerName")
    @Expose
    private String fromAccountOwnerName;
    @SerializedName("FromAccount_IBAN")
    @Expose
    private String fromAccountIBAN;

    public String getToAccountNumber() {
        return toAccountNumber;
    }

    public void setToAccountNumber(String toAccountNumber) {
        this.toAccountNumber = toAccountNumber;
    }

    public String getToAccountOwnerName() {
        return toAccountOwnerName;
    }

    public void setToAccountOwnerName(String toAccountOwnerName) {
        this.toAccountOwnerName = toAccountOwnerName;
    }

    public String getToBankName() {
        return toBankName;
    }

    public void setToBankName(String toBankName) {
        this.toBankName = toBankName;
    }

    public String getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }

    public Integer getTransferedAmount() {
        return transferedAmount;
    }

    public void setTransferedAmount(Integer transferedAmount) {
        this.transferedAmount = transferedAmount;
    }

    public Object getTransferImage() {
        return transferImage;
    }

    public void setTransferImage(Object transferImage) {
        this.transferImage = transferImage;
    }

    public String getTransferNumber() {
        return transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getFromBankName() {
        return fromBankName;
    }

    public void setFromBankName(String fromBankName) {
        this.fromBankName = fromBankName;
    }

    public String getFromAccountNumber() {
        return fromAccountNumber;
    }

    public void setFromAccountNumber(String fromAccountNumber) {
        this.fromAccountNumber = fromAccountNumber;
    }

    public String getFromAccountOwnerName() {
        return fromAccountOwnerName;
    }

    public void setFromAccountOwnerName(String fromAccountOwnerName) {
        this.fromAccountOwnerName = fromAccountOwnerName;
    }

    public String getFromAccountIBAN() {
        return fromAccountIBAN;
    }

    public void setFromAccountIBAN(String fromAccountIBAN) {
        this.fromAccountIBAN = fromAccountIBAN;
    }

}