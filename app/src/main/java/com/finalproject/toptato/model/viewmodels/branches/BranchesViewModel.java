package com.finalproject.toptato.model.viewmodels.branches;

import android.app.Application;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.entities.api.MessageResponse;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;
import io.reactivex.observables.GroupedObservable;

public class BranchesViewModel extends BaseViewModel {
    private final String TAG = getClass().getSimpleName();

    private MutableLiveData<Map<Integer, List<Branch>>> branchesMapMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Location> currentLocationMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<String[]> countryNameLiveData = new MutableLiveData<>();
    private MutableLiveData<List<Branch>> branchListLiveData = new MutableLiveData<>();
    private List<Integer> branchKeyId;


    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult != null) {
                currentLocationMutableLiveData.setValue(locationResult.getLastLocation());
                fusedLocationClient.removeLocationUpdates(this);
            }

        }
    };

    public BranchesViewModel(@NonNull Application application) {
        super(application);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(application);
    }

    public MutableLiveData<Map<Integer, List<Branch>>> getBranchesMapMutableLiveData() {
        return branchesMapMutableLiveData;
    }

    public MutableLiveData<Location> getCurrentLocationMutableLiveData() {
        return currentLocationMutableLiveData;
    }

    public MutableLiveData<String[]> getCountryNameLiveData() {
        return countryNameLiveData;
    }

    public MutableLiveData<List<Branch>> getBranchListLiveData() {
        return branchListLiveData;
    }

    /**
     * send request api to server and receive all branch data
     * and convert it to map of data and set value to mutableLiveData
     */
    public MutableLiveData<String[]> getBranches() {
        setIsLoading(true);
        getRepository()
                .getBranches()
                .flatMapIterable(new Function<MessageResponse<Branch>, Iterable<Branch>>() {
                    @Override
                    public Iterable<Branch> apply(MessageResponse<Branch> branchesResponse) throws Exception {
                        return branchesResponse != null ? branchesResponse.getDataList() : null;
                    }
                })
                .groupBy(new Function<Branch, Integer>() {
                    @Override
                    public Integer apply(Branch branchesLstItem) throws Exception {
                        return branchesLstItem.getCountryId();
                    }
                })
                .flatMapSingle(new Function<GroupedObservable<Integer, Branch>, SingleSource<List<Branch>>>() {
                    @Override
                    public SingleSource<List<Branch>> apply(GroupedObservable<Integer, Branch> integerBranchesLstItemGroupedObservable) throws Exception {
                        return integerBranchesLstItemGroupedObservable
                                .collect(new Callable<List<Branch>>() {
                                             @Override
                                             public List<Branch> call() throws Exception {
                                                 return new ArrayList<Branch>();
                                             }
                                         }
                                        , new BiConsumer<List<Branch>, Branch>() {
                                            @Override
                                            public void accept(List<Branch> branchesLstItems, Branch branchesLstItem) throws Exception {
                                                branchesLstItems.add(branchesLstItem);
                                            }
                                        }
                                );
                    }
                })
                .toMap(new Function<List<Branch>, Integer>() {
                    @Override
                    public Integer apply(List<Branch> branchesLstItems) throws Exception {
                        return branchesLstItems.get(0).getCountryId();
                    }
                })
                .subscribe(new SingleObserver<Map<Integer, List<Branch>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "onSubscribe: 11 ");
                    }

                    @Override
                    public void onSuccess(Map<Integer, List<Branch>> integerListMap) {
                        Log.i(TAG, "onSuccess: 11");
                        setIsLoading(false);
                        getCountryName(integerListMap);
                        branchesMapMutableLiveData.setValue(integerListMap);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: " + e.getMessage());
                        setIsLoading(false);
                    }
                });

        return countryNameLiveData;

    }

    /**
     * create location request and sent it
     */
    public void getCurrentLocation() {

        Log.i(TAG, "getCurrentLocation: ");

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());


    }

    /**
     * return country names in array string
     *
     * @param branchMap
     * @return string of array whose the user will select her country
     */
    public String[] getCountryName(Map<Integer, List<Branch>> branchMap) {
        if (branchMap == null || branchMap.isEmpty()) return new String[0];
         branchKeyId = new ArrayList<>(branchMap.keySet());


        String[] countryNamesArray = new String[branchKeyId.size()];

        Log.i(TAG, "onChanged: is callled");

        //get name of country from map data and save in countryNamesArray
        List<Branch> branchesLstItem;
        for (int x = 0; x < branchKeyId.size(); x++) {
            branchesLstItem = branchMap.get(branchKeyId.get(x));
            if (branchesLstItem == null) continue;

            countryNamesArray[x] = branchesLstItem.get(0).getCountryName(getApplication());
        }

        countryNameLiveData.setValue(countryNamesArray);

        return countryNamesArray;
    }


    public String  updateListBranches(int which) {
        if (which==-1)return " ";

        Map<Integer, List<Branch>> map=branchesMapMutableLiveData.getValue();

        if (map==null||countryNameLiveData.getValue()==null)return "" ;

        branchListLiveData.setValue(map.get(branchKeyId.get(which)));

        return countryNameLiveData.getValue()[branchKeyId.get(which)];
    }
}
