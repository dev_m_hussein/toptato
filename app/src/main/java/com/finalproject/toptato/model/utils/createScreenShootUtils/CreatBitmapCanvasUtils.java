package com.finalproject.toptato.model.utils.createScreenShootUtils;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;

import com.finalproject.toptato.model.entities.api.ScreenShootBitmap;

import java.util.List;

/**
 * Created by mahmoud saad
 */
public class CreatBitmapCanvasUtils {
    private final String TAG = getClass().getSimpleName();
    private Bitmap mainBitmap, childBitmap;
    private Canvas mainCanvas;
    private Paint paint;
    private Rect rect;
    private int padding = 16, mainWidth, mainHeight, currentHeight = 0, selectedColor = 9, selectedSize = 5;
    private Shader linearGradient;
    private boolean isRTLDirection = true;
    private TextPaint textPaint = new TextPaint();
    private List<Integer> colorList;
    private List<String> sizeList, colorNameList;
    private String descriptionText, codeNumberItem;
    private Activity mActivity;
    private List<Bitmap> bitmapList;
    private ScreenShootBitmap screenShootBitmap;


    public CreatBitmapCanvasUtils(Activity context, ScreenShootBitmap screenShootBitmap) {
        this.screenShootBitmap = screenShootBitmap;
        mActivity = context;
        initProduct();
    }

    private void initProduct() {
        DisplayMetrics displayMetrics = new DisplayMetrics();

        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        mainHeight = displayMetrics.heightPixels;
        mainWidth = displayMetrics.widthPixels;

        rect = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);

        mainBitmap = Bitmap.createBitmap(mainWidth, mainHeight, Bitmap.Config.ARGB_8888);

        mainCanvas = new Canvas(mainBitmap);

        initValues();

    }

    private void initValues() {

        paint = new Paint();
        paint.setAntiAlias(true);

        bitmapList = screenShootBitmap.getImageBitmapList();

        linearGradient = new LinearGradient(
                0,
                0,
                mainWidth,
                mainHeight,
                Color.WHITE,
                Color.parseColor("#EFE7DA"),
                Shader.TileMode.CLAMP);

        colorList = screenShootBitmap.getColorValueList();

        sizeList = screenShootBitmap.getSizeItemList();

        descriptionText = screenShootBitmap.getDescriptionItemText();

        colorNameList = screenShootBitmap.getColorNameList();

        isRTLDirection = screenShootBitmap.isDirectionRTL();

        codeNumberItem = screenShootBitmap.getCodeNumberItem();
        selectedColor = screenShootBitmap.getIndexSelectedColor();
        selectedSize = screenShootBitmap.getIndexSelectedSize();

    }


    public Bitmap createScreenBitmap() {
        setVerticalGap(padding);

        //draw background color
        paint.setShader(linearGradient);
        mainCanvas.drawRect(rect, paint);
        paint.setShader(null);

        //draw images
        currentHeight += drawImages(mainWidth / 3, mainHeight / 4, bitmapList);

        setVerticalGap(mainHeight / 42);

        //draw item number
        drawItemNubmer(isRTLDirection ? "رقم الصنف" : "Item Number", codeNumberItem);

        //set space between views
        setVerticalGap(mainHeight / 42);

        drawLine();


        //set space between views
        setVerticalGap(mainHeight / 42);


        padding = 20;

        Log.i(TAG, "createScreenBitmap:1 " + currentHeight);
        //draw all color
        drawCircleColor(isRTLDirection ? "الألوان" : "Colors", colorList, colorNameList);

        Log.i(TAG, "createScreenBitmap:2 " + currentHeight);

        //set space between views
        setVerticalGap(mainHeight / 22);


        drawLine();


        //set space between views
        setVerticalGap(mainHeight / 42);


        //draw rectangle
        drawRectSize(isRTLDirection ? "المقاس المتاح" : "Sizes offers ", sizeList);

        //set space between views
        setVerticalGap(mainHeight / 42);


        //draw line
        drawLine();

        //set space between views
        setVerticalGap(mainHeight / 42);


        drawDescription(isRTLDirection ? "الوصف" : "The description");

        return mainBitmap;

    }

    private void setVerticalGap(int spaceGap) {
        currentHeight += spaceGap;
    }

    private void drawItemNubmer(String codeItemTite, String code) {
        paint.setColor(Color.parseColor("#C0AA7B"));
        paint.setTextSize(44);
        drawHeader(codeItemTite + "  " + code, currentHeight, padding, paint, isRTLDirection);


    }

    private void drawDescription(String descriptionTitle) {
        paint.setTextSize(44);
        paint.setColor(Color.BLACK);
        drawHeader(descriptionTitle, currentHeight, padding, paint, isRTLDirection);


        currentHeight += padding * 2;
        mainCanvas.translate(padding, currentHeight);

        paint.setTextSize(23);
        paint.setColor(Color.BLACK);
        StaticLayout sl;
        textPaint.setTextSize(32);
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.parseColor("#848486"));
        if (descriptionText == null) descriptionText = " ";
        sl = new StaticLayout(descriptionText, textPaint, mainWidth - 4 * padding,
                Layout.Alignment.ALIGN_CENTER, 1, 8, true);
        sl.draw(mainCanvas);
    }

    private void drawHeader(String header, int height, int padding, Paint paint, boolean isRTLDirection) {

        drawText(
                mainCanvas,
                header,
                paint,
                Math.abs((isRTLDirection ? mainWidth : 0) - padding), height, isRTLDirection);

    }

    private void drawLine() {
        //draw line
        paint.setStrokeWidth(1);
        paint.setColor(Color.parseColor("#D8D8D8"));

        mainCanvas.drawLine(0, currentHeight, mainWidth, currentHeight, paint);
        currentHeight++;
    }

    private void drawCircleColor(String headerColor, List<Integer> colorList, List<String> colorNameList) {
        int paddingCircle = mainWidth / 36;
        paint.setTextSize(44);
        paint.setColor(Color.BLACK);

        drawHeader(headerColor, currentHeight, paddingCircle, paint, isRTLDirection);

        paint.setTextSize(22);

        drawListColor(paddingCircle, colorList, colorNameList);

        currentHeight += paddingCircle;


    }

    private void drawListColor(int paddingCircle, List<Integer> colorList, List<String> colorNameList) {
        int count = 0;

        int size = colorList.size();
        for (int i = 0; i < size; i++) {
            if (i % 6 == 0 && i != 0) {
                currentHeight += 90;
                count = 0;
            }
            int x = paddingCircle * 3 + paddingCircle * 4 * count;

            if (i == selectedColor) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(Color.BLACK);
                paint.setStrokeWidth(4);
                mainCanvas.drawCircle(Math.abs((isRTLDirection ? 0 : mainWidth) - x), currentHeight - paddingCircle, mainHeight / 44, paint);

            }

            paint.setStyle(Paint.Style.FILL);
            paint.setColor(colorList.get(i));
            mainCanvas.drawCircle(Math.abs((isRTLDirection ? 0 : mainWidth) - x), currentHeight - paddingCircle, mainHeight / 53, paint);
            paint.setColor(Color.parseColor(i == selectedColor ? "#000000" : "#686667"));
            drawCenterText(mainCanvas, colorNameList.get(i), paint, Math.abs((isRTLDirection ? 0 : mainWidth) - x), currentHeight + paddingCircle, size - 1 == i);

            count++;

        }
    }


    private void drawRectSize(String headerSize, List<String> sizeList) {

        int paddingRect = mainWidth / 36;
        paint.setColor(Color.BLACK);
        paint.setTextSize(44);

        Log.i(TAG, "drawRectSize: 3 " + currentHeight);
        drawHeader(headerSize, currentHeight, paddingRect, paint, isRTLDirection);


        drawListSize(paddingRect, sizeList);


    }

    private void drawListSize(int paddingRect, List<String> sizeList) {
        paint.setTextSize(17);

        int size = sizeList.size(), count = 0;
        for (int i = 0; i < size; i++) {

            if (i % 5 == 0 && i != 0) {
                currentHeight += 70;
                count = 0;
            }
            int x = paddingRect * 4 + paddingRect * 3 * count, y = currentHeight - paddingRect * 2, rectWidth = mainWidth / 16;

            paint.setColor(Color.parseColor(i == selectedSize ? "#C1AB7C" : "#DDDAD5"));


            paint.setStyle(Paint.Style.FILL);
            mainCanvas.drawRoundRect(Math.abs((isRTLDirection ? 0 : mainWidth) - x), y, Math.abs((isRTLDirection ? 0 : mainWidth) - x + rectWidth), y + rectWidth, 9f, 9f, paint);
            paint.setColor(Color.BLACK);
            drawCenterText(mainCanvas, sizeList.get(i), paint, Math.abs((isRTLDirection ? 0 : mainWidth) - x + rectWidth / 2), y + rectWidth / 2, isRTLDirection);


            count++;

        }
    }


    private void drawText(Canvas canvas, String text, Paint paint, int x, int y, boolean isRTLDirection) {
        paint.setTextAlign(isRTLDirection ? Paint.Align.RIGHT : Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), rect);

        canvas.drawText(text, x, y + rect.height(), paint);

        currentHeight += rect.height() + y - currentHeight;

    }

    private void drawCenterText(Canvas canvas, String text, Paint paint, int x, int y, boolean isArabic) {
        paint.setTextAlign(isArabic ? Paint.Align.RIGHT : Paint.Align.LEFT);

        paint.getTextBounds(text, 0, text.length(), rect);

        canvas.drawText(text, x - (isArabic ? rect.exactCenterX() * -1 : rect.exactCenterX()), y - rect.exactCenterY(), paint);


    }


    private int drawImages(int width, int height, List<Bitmap> bitmaps) {

        for (int i = 0; i < bitmaps.size(); i++) {
            rect = new Rect(padding + width * i, padding, (i + 1) * width - padding, height - padding);
            mainCanvas.drawBitmap(bitmaps.get(i), null, rect, paint);
        }

        return rect.height();
    }

}
