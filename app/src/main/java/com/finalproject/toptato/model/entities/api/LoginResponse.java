package com.finalproject.toptato.model.entities.api;

import com.google.gson.annotations.SerializedName;

public class LoginResponse extends MessageResponse {

    @SerializedName("Customer_Id")
    private int customerId;

    @SerializedName("RefNumber")
    private int refNumber;

    @SerializedName("CustomerName")
    private String customerName;

    @SerializedName("Image")
    private String image;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(int refNumber) {
        this.refNumber = refNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}