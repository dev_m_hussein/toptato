package com.finalproject.toptato.model.entities.api.itemdetailsModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatrixListItem{

	@SerializedName("SizeArabicName")
	private String sizeArabicName;

	@SerializedName("SizeEnglishName")
	private String sizeEnglishName;

	@SerializedName("SizeId")
	private int sizeId;

	@SerializedName("colors")
	private List<ColorsItem> colors;

	public void setSizeArabicName(String sizeArabicName){
		this.sizeArabicName = sizeArabicName;
	}

	public String getSizeArabicName(){
		return sizeArabicName;
	}

	public void setSizeEnglishName(String sizeEnglishName){
		this.sizeEnglishName = sizeEnglishName;
	}

	public String getSizeEnglishName(){
		return sizeEnglishName;
	}

	public void setSizeId(int sizeId){
		this.sizeId = sizeId;
	}

	public int getSizeId(){
		return sizeId;
	}

	public void setColors(List<ColorsItem> colors){
		this.colors = colors;
	}

	public List<ColorsItem> getColors(){
		return colors;
	}

	@Override
 	public String toString(){
		return 
			"MatrixListItem{" + 
			"sizeArabicName = '" + sizeArabicName + '\'' + 
			",sizeEnglishName = '" + sizeEnglishName + '\'' + 
			",sizeId = '" + sizeId + '\'' + 
			",colors = '" + colors + '\'' + 
			"}";
		}
}