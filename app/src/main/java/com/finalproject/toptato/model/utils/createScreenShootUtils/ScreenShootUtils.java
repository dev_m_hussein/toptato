package com.finalproject.toptato.model.utils.createScreenShootUtils;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import com.finalproject.toptato.model.entities.api.ScreenShootBitmap;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.ColorsItem;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.ImagesListItem;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.ItemListItem;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.MatrixListItem;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahmoud saad
 */

public class ScreenShootUtils {

    public static ScreenShootBitmap prepareScreenShootDetail(ItemListItem item, boolean isDirectionRTL, int selectedSize, int selectedColor) {
        ScreenShootBitmap screenShootBitmap = new ScreenShootBitmap();

        isDirectionRTL = !isDirectionRTL;
        screenShootBitmap.setDirectionRTL(isDirectionRTL);
        screenShootBitmap.setDescriptionItemText(isDirectionRTL ? item.getArabicDetails() : item.getDescEN());
        screenShootBitmap.setCodeNumberItem(item.getItemCode());
        screenShootBitmap.setSizeItemList(getSizeListBasedOnLanguage(item.getMatrixList(), isDirectionRTL));
        screenShootBitmap.setIndexSelectedSize(selectedSize);
        screenShootBitmap.setIndexSelectedColor(selectedColor);
        screenShootBitmap.setColorNameList(getColorNameListBasedOnLanguage(item.getMatrixList().get(0).getColors(), isDirectionRTL));
        screenShootBitmap.setColorValueList(getColorValueList(item.getMatrixList().get(0).getColors()));

        screenShootBitmap.setImageBitmapList(prepareScreenShootImages(item.getImagesList()));

        return screenShootBitmap;
    }

    public static List<Bitmap> prepareScreenShootImages(List<ImagesListItem> imagesList) {
        if (imagesList == null) return null;

        while (imagesList.size() < 3) {
            imagesList.add(imagesList.get(0));
        }
        List<Bitmap> bitmapList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            try {
                bitmapList.add(Picasso.get().load(imagesList.get(i).getImagePath()).get());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmapList;

    }

    public static List<String> getSizeListBasedOnLanguage(List<MatrixListItem> listItems, boolean isDirectionRTL) {
        List<String> sizeList = new ArrayList<>();
        for (MatrixListItem item : listItems) {
            Log.i("mano", "getSizeListBasedOnLanguage: " + item.toString());
            sizeList.add(isDirectionRTL ? item.getSizeEnglishName() : item.getSizeEnglishName());
        }
        return sizeList;
    }

    private static List<String> getColorNameListBasedOnLanguage(List<ColorsItem> listItems, boolean isDirectionRTL) {
        List<String> colorList = new ArrayList<>();

        for (ColorsItem item : listItems) {
            colorList.add(isDirectionRTL ? item.getColorArabicName() : item.getColorEnglishName());
        }
        return colorList;
    }

    private static List<Integer> getColorValueList(List<ColorsItem> listItems) {
        List<Integer> colorList = new ArrayList<>();

        for (ColorsItem item : listItems) {
            colorList.add(Color.parseColor(item.getColorCode()));
        }
        return colorList;
    }

}
