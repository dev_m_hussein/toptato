package com.finalproject.toptato.model.entities.api.cartitemdetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CartItemDetailsResponse{

	@SerializedName("CurrencyRate")
	private double currencyRate;

	@SerializedName("PaymentDate")
	private String paymentDate;

	@SerializedName("PaymentRefNum")
	private String paymentRefNum;

	@SerializedName("TaxValue")
	private double taxValue;

	@SerializedName("TBL_OrderItems")
	private List<TBLOrderItemsItem> tBLOrderItems;

	@SerializedName("CustomerId")
	private int customerId;

	@SerializedName("Code")
	private String code;

	@SerializedName("CurrencyId")
	private int currencyId;

	@SerializedName("CustomerDiscount")
	private double customerDiscount;

	@SerializedName("OrderDate")
	private String orderDate;

	@SerializedName("ShippingMethodId")
	private int shippingMethodId;

	@SerializedName("TotalDiscountValue")
	private double totalDiscountValue;

	@SerializedName("TotalValue")
	private double totalValue;

	@SerializedName("CustomerAddressId")
	private int customerAddressId;

	@SerializedName("CustomerComment")
	private String customerComment;

	@SerializedName("NetValue")
	private double netValue;

	@SerializedName("PaymentStatusId")
	private int paymentStatusId;

	@SerializedName("ShippingValue")
	private double shippingValue;

	@SerializedName("SubTotalValue")
	private double subTotalValue;

	@SerializedName("OrderStatusId")
	private int orderStatusId;

	@SerializedName("PaymentMethodsId")
	private int paymentMethodsId;

	@SerializedName("InvoiceDiscountValue")
	private double invoiceDiscountValue;

	public void setCurrencyRate(double currencyRate){
		this.currencyRate = currencyRate;
	}

	public double getCurrencyRate(){
		return currencyRate;
	}

	public void setPaymentDate(String paymentDate){
		this.paymentDate = paymentDate;
	}

	public String getPaymentDate(){
		return paymentDate;
	}

	public void setPaymentRefNum(String paymentRefNum){
		this.paymentRefNum = paymentRefNum;
	}

	public String getPaymentRefNum(){
		return paymentRefNum;
	}

	public void setTaxValue(double taxValue){
		this.taxValue = taxValue;
	}

	public double getTaxValue(){
		return taxValue;
	}

	public void setTBLOrderItems(List<TBLOrderItemsItem> tBLOrderItems){
		this.tBLOrderItems = tBLOrderItems;
	}

	public List<TBLOrderItemsItem> getTBLOrderItems(){
		return tBLOrderItems;
	}

	public void setCustomerId(int customerId){
		this.customerId = customerId;
	}

	public int getCustomerId(){
		return customerId;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setCurrencyId(int currencyId){
		this.currencyId = currencyId;
	}

	public int getCurrencyId(){
		return currencyId;
	}

	public void setCustomerDiscount(double customerDiscount){
		this.customerDiscount = customerDiscount;
	}

	public double getCustomerDiscount(){
		return customerDiscount;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setShippingMethodId(int shippingMethodId){
		this.shippingMethodId = shippingMethodId;
	}

	public int getShippingMethodId(){
		return shippingMethodId;
	}

	public void setTotalDiscountValue(double totalDiscountValue){
		this.totalDiscountValue = totalDiscountValue;
	}

	public double getTotalDiscountValue(){
		return totalDiscountValue;
	}

	public void setTotalValue(double totalValue){
		this.totalValue = totalValue;
	}

	public double getTotalValue(){
		return totalValue;
	}

	public void setCustomerAddressId(int customerAddressId){
		this.customerAddressId = customerAddressId;
	}

	public int getCustomerAddressId(){
		return customerAddressId;
	}

	public void setCustomerComment(String customerComment){
		this.customerComment = customerComment;
	}

	public String getCustomerComment(){
		return customerComment;
	}

	public void setNetValue(double netValue){
		this.netValue = netValue;
	}

	public double getNetValue(){
		return netValue;
	}

	public void setPaymentStatusId(int paymentStatusId){
		this.paymentStatusId = paymentStatusId;
	}

	public int getPaymentStatusId(){
		return paymentStatusId;
	}

	public void setShippingValue(double shippingValue){
		this.shippingValue = shippingValue;
	}

	public double getShippingValue(){
		return shippingValue;
	}

	public void setSubTotalValue(double subTotalValue){
		this.subTotalValue = subTotalValue;
	}

	public double getSubTotalValue(){
		return subTotalValue;
	}

	public void setOrderStatusId(int orderStatusId){
		this.orderStatusId = orderStatusId;
	}

	public int getOrderStatusId(){
		return orderStatusId;
	}

	public void setPaymentMethodsId(int paymentMethodsId){
		this.paymentMethodsId = paymentMethodsId;
	}

	public int getPaymentMethodsId(){
		return paymentMethodsId;
	}

	public void setInvoiceDiscountValue(double invoiceDiscountValue){
		this.invoiceDiscountValue = invoiceDiscountValue;
	}

	public double getInvoiceDiscountValue(){
		return invoiceDiscountValue;
	}

	@Override
 	public String toString(){
		return 
			"CartItemDetailsResponse{" + 
			"currencyRate = '" + currencyRate + '\'' + 
			",paymentDate = '" + paymentDate + '\'' + 
			",paymentRefNum = '" + paymentRefNum + '\'' + 
			",taxValue = '" + taxValue + '\'' + 
			",tBL_OrderItems = '" + tBLOrderItems + '\'' + 
			",customerId = '" + customerId + '\'' + 
			",code = '" + code + '\'' + 
			",currencyId = '" + currencyId + '\'' + 
			",customerDiscount = '" + customerDiscount + '\'' + 
			",orderDate = '" + orderDate + '\'' + 
			",shippingMethodId = '" + shippingMethodId + '\'' + 
			",totalDiscountValue = '" + totalDiscountValue + '\'' + 
			",totalValue = '" + totalValue + '\'' + 
			",customerAddressId = '" + customerAddressId + '\'' + 
			",customerComment = '" + customerComment + '\'' + 
			",netValue = '" + netValue + '\'' + 
			",paymentStatusId = '" + paymentStatusId + '\'' + 
			",shippingValue = '" + shippingValue + '\'' + 
			",subTotalValue = '" + subTotalValue + '\'' + 
			",orderStatusId = '" + orderStatusId + '\'' + 
			",paymentMethodsId = '" + paymentMethodsId + '\'' + 
			",invoiceDiscountValue = '" + invoiceDiscountValue + '\'' + 
			"}";
		}
}