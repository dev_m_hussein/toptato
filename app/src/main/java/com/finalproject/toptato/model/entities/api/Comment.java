package com.finalproject.toptato.model.entities.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerIMG")
    @Expose
    private String customerIMG;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("ReviewStars")
    @Expose
    private Integer reviewStars;
    @SerializedName("IsApproved")
    @Expose
    private Boolean isApproved;
    @SerializedName("CreationDate")
    @Expose
    private String creationDate;
    @SerializedName("AppoveDate")
    @Expose
    private String appoveDate;
    @SerializedName("ApprovedBy")
    @Expose
    private Object approvedBy;
    @SerializedName("ApprovedByName")
    @Expose
    private String approvedByName;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerIMG() {
        return customerIMG;
    }

    public void setCustomerIMG(String customerIMG) {
        this.customerIMG = customerIMG;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(Integer reviewStars) {
        this.reviewStars = reviewStars;
    }

    public Boolean isApproved() {
        return isApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        this.isApproved = isApproved;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getAppoveDate() {
        return appoveDate;
    }

    public void setAppoveDate(String appoveDate) {
        this.appoveDate = appoveDate;
    }

    public Object getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Object approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByName() {
        return approvedByName;
    }

    public void setApprovedByName(String approvedByName) {
        this.approvedByName = approvedByName;
    }
}
