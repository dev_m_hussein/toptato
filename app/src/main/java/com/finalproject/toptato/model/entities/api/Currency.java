package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Currency {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("ArabicUnitName")
    @Expose
    private String arabicUnitName;
    @SerializedName("EnglishUnitName")
    @Expose
    private String englishUnitName;
    @SerializedName("CurrentRate")
    @Expose
    private Double currentRate;
    @SerializedName("UnitRate")
    @Expose
    private Integer unitRate;
    @SerializedName("Symbol")
    @Expose
    private String symbol;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getArabicUnitName() {
        return arabicUnitName;
    }

    public void setArabicUnitName(String arabicUnitName) {
        this.arabicUnitName = arabicUnitName;
    }

    public String getEnglishUnitName() {
        return englishUnitName;
    }

    public void setEnglishUnitName(String englishUnitName) {
        this.englishUnitName = englishUnitName;
    }

    public Double getCurrentRate() {
        return currentRate;
    }

    public void setCurrentRate(Double currentRate) {
        this.currentRate = currentRate;
    }

    public Integer getUnitRate() {
        return unitRate;
    }

    public void setUnitRate(Integer unitRate) {
        this.unitRate = unitRate;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicName() : getEnglishName();
    }

    public String getUnitName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicUnitName() : getEnglishUnitName();
    }

}
