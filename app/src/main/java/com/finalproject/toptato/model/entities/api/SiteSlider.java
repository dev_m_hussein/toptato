package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SiteSlider {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ArabicTitle")
    @Expose
    private String arabicTitle;
    @SerializedName("EnglishTitle")
    @Expose
    private String englishTitle;
    @SerializedName("ContentAR")
    @Expose
    private String contentArabic;
    @SerializedName("ContentEN")
    @Expose
    private String contentEnglish;
    @SerializedName("HeaderAR")
    @Expose
    private String headerArabic;
    @SerializedName("HeaderEN")
    @Expose
    private String headerEnglish;
    @SerializedName("Sequence")
    @Expose
    private Integer sequence;
    @SerializedName("StoryImage")
    @Expose
    private String storyImage;
    @SerializedName("BackGroundImage")
    @Expose
    private String backgroundImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicTitle() {
        return arabicTitle;
    }

    public void setArabicTitle(String arabicTitle) {
        this.arabicTitle = arabicTitle;
    }

    public String getEnglishTitle() {
        return englishTitle;
    }

    public void setEnglishTitle(String englishTitle) {
        this.englishTitle = englishTitle;
    }

    public String getContentArabic() {
        return contentArabic;
    }

    public void setContentArabic(String contentArabic) {
        this.contentArabic = contentArabic;
    }

    public String getContentEnglish() {
        return contentEnglish;
    }

    public void setContentEnglish(String contentEnglish) {
        this.contentEnglish = contentEnglish;
    }

    public String getHeaderArabic() {
        return headerArabic;
    }

    public void setHeaderArabic(String headerArabic) {
        this.headerArabic = headerArabic;
    }

    public String getHeaderEnglish() {
        return headerEnglish;
    }

    public void setHeaderEnglish(String headerEnglish) {
        this.headerEnglish = headerEnglish;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getStoryImage() {
        return storyImage;
    }

    public void setStoryImage(String storyImage) {
        this.storyImage = storyImage;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getTitle(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicTitle() : getEnglishTitle();
    }

    public String getContent(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getContentArabic() : getContentEnglish();
    }

    public String getHeader(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getHeaderArabic() : getHeaderEnglish();
    }
}
