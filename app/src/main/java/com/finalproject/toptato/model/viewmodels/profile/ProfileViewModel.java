package com.finalproject.toptato.model.viewmodels.profile;


import android.app.Application;

import androidx.annotation.NonNull;

import com.finalproject.toptato.model.entities.profile.ProfileView;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;

public class ProfileViewModel extends BaseViewModel {

    private ProfileView profileView;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
    }


    public void setProfileView(ProfileView profileView) {
        this.profileView = profileView;
    }

    public void startSignUpActivity() {
        profileView.startSignUp();
    }

    public void startLogInActivity() {
        profileView.startLogIn();
    }
}
