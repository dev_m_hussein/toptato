package com.finalproject.toptato.model.utils.validation;

/**
 * Created by mahmoud saad
 */
public class PasswordValid extends EmptyValid {

    @Override
    public boolean valid(String str) {
        return super.valid(str)&& str.length() > 6;
    }
}
