package com.finalproject.toptato.model.viewmodels.map;

import android.app.Application;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.utils.GpsProviderHelper;
import com.finalproject.toptato.model.utils.SingleLiveEvent;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahmoud saad
 */

public class MapViewModel extends BaseViewModel {
    private final String TAG = getClass().getSimpleName();

    private MutableLiveData<Location> currentLocationMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<List<Branch>> branchesMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<String> distanceMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Branch> selectedBranchMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> prepareCurrentLocationMutableLiveData = new SingleLiveEvent<>();
    private MutableLiveData<List<LatLng>> listLatLangLiveData = new MutableLiveData<>();
    private MutableLiveData<String> startNavigationMapMutableLiveData = new SingleLiveEvent<>();


    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult != null) {
                currentLocationMutableLiveData.setValue(locationResult.getLastLocation());
                calculateDistance();

                if (prepareCurrentLocationMutableLiveData.getValue() != null
                        &&prepareCurrentLocationMutableLiveData.getValue()) {
                    goNavigationGoogleMap();
                }

                fusedLocationClient.removeLocationUpdates(this);
            }
            Log.i(TAG, "onLocationResult: " + locationResult.toString());

        }
    };


    public MapViewModel(@NonNull Application application) {
        super(application);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(application);

    }

    public MutableLiveData<Location> getCurrentLocationMutableLiveData() {
        return currentLocationMutableLiveData;
    }

    public MutableLiveData<List<Branch>> getBranchesMutableLiveData() {
        return branchesMutableLiveData;
    }

    public MutableLiveData<String> getDistanceMutableLiveData() {
        return distanceMutableLiveData;
    }

    public MutableLiveData<Branch> getSelectedBranchMutableLiveData() {
        return selectedBranchMutableLiveData;
    }

    public MutableLiveData<Boolean> getPrepareCurrentLocationMutableLiveData() {
        return prepareCurrentLocationMutableLiveData;
    }

    public MutableLiveData<String> getStartNavigationMapMutableLiveData() {
        return startNavigationMapMutableLiveData;
    }

    public MutableLiveData<List<LatLng>> getListLatLangLiveData() {
        return listLatLangLiveData;
    }

    /**
     * get all branches from intent and set value to branchesMutableLiveData
     *
     * @param intent
     * @return
     */
    public void prepareBranchesIntent(Intent intent) {
        if (intent == null) return ;

        List<Branch> branchesLstItemList = new ArrayList<>();

        if (intent.hasExtra(getApplication().getString(R.string.branch_selected_key))) {
            Branch branchesLstItem = (Branch) intent.getSerializableExtra(getApplication().getString(R.string.branch_selected_key));
            if (branchesLstItem != null) {
                branchesLstItemList.add(branchesLstItem);

            }
        } else if (intent.hasExtra(getApplication().getString(R.string.branches_selected_key))) {
            if ( intent.getSerializableExtra(getApplication().getString(R.string.branches_selected_key))!=null)
            branchesLstItemList= (List<Branch>) intent.getSerializableExtra(getApplication().getString(R.string.branches_selected_key));


        }

        branchesMutableLiveData.setValue(branchesLstItemList);

        convertToLatlng(branchesLstItemList);

    }

    public void prepareLocationIntent(Intent intent) {
        if (intent == null || currentLocationMutableLiveData == null) return;

        String latitudeKey = getApplication().getResources().getString(R.string.location_latitude_key), longitudeKey = getApplication().getResources().getString(R.string.location_longitude_key);

        if (intent.hasExtra(latitudeKey) && intent.hasExtra(longitudeKey)) {

            Location location = new Location("");

            location.setLatitude(intent.getDoubleExtra(latitudeKey, 0));
            location.setLongitude(intent.getDoubleExtra(longitudeKey, 0));


            currentLocationMutableLiveData.setValue(location);
            calculateDistance();
        }
    }

    /**
     * convert list of Branch  to list of LatLng
     *
     * @param branchesLstItemList list of all branches
     * @return list of LatLng
     */
     void convertToLatlng(List<Branch> branchesLstItemList) {
        if (branchesLstItemList == null) return ;

        List<LatLng> latLngList = new ArrayList<>();
        for (Branch branch : branchesLstItemList) {
            latLngList.add(branch.getLocation());
        }

        listLatLangLiveData.setValue(latLngList);

    }

    /**
     * create LocationRequest and sent it
     */
    public void getCurrentLocation() {

        Log.i(TAG, "getCurrentLocation: ");

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());


    }

    /**
     * calculate distance from my location and branch location and set value to mutableLiveData
     */
    public void calculateDistance() {
        if (selectedBranchMutableLiveData.getValue() == null || currentLocationMutableLiveData.getValue() == null)
            return;

        Location distLocation = new Location("");
        distLocation.setLatitude(Double.valueOf(selectedBranchMutableLiveData.getValue().getXPiont()));
        distLocation.setLatitude(Double.valueOf(selectedBranchMutableLiveData.getValue().getYPiont()));
        getDistanceMutableLiveData()
                .setValue(
                        GpsProviderHelper.calculateDistance(currentLocationMutableLiveData.getValue(), distLocation)
                                + ""
                );
    }

    /**
     * send tow location (my location , branch location) to google map navigation
     */
    public void goNavigationGoogleMap() {

        if (selectedBranchMutableLiveData.getValue() == null) return;
        if (currentLocationMutableLiveData.getValue() == null) {
            prepareCurrentLocationMutableLiveData.setValue(true);
            return;
        }

        LatLng distLatLng = new LatLng(
                Double.valueOf(selectedBranchMutableLiveData.getValue().getXPiont()),
                Double.valueOf(selectedBranchMutableLiveData.getValue().getYPiont())
        );
        LatLng sourceLatLng = new LatLng(
                currentLocationMutableLiveData.getValue().getLatitude(),
                currentLocationMutableLiveData.getValue().getLongitude()
        );
        String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="
                + sourceLatLng.latitude + "," + sourceLatLng.longitude +
                "&daddr=" + distLatLng.latitude + "," + distLatLng.longitude;
        startNavigationMapMutableLiveData.setValue(uri);


    }


}
