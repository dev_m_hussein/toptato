package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact extends MessageResponse {

    @SerializedName("Instagram")
    @Expose
    private String instagram;
    @SerializedName("Facebook")
    @Expose
    private String facebook;
    @SerializedName("WhatsUp")
    @Expose
    private String whatsUp;
    @SerializedName("Google")
    @Expose
    private String google;
    @SerializedName("Twitter")
    @Expose
    private String twitter;
    @SerializedName("LinkedIn")
    @Expose
    private String linkedIn;
    @SerializedName("YouTube")
    @Expose
    private String youTube;
    @SerializedName("PayInReceiveAmount")
    @Expose
    private String payInReceiveAmount;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWhatsUp() {
        return whatsUp;
    }

    public void setWhatsUp(String whatsUp) {
        this.whatsUp = whatsUp;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getYouTube() {
        return youTube;
    }

    public void setYouTube(String youTube) {
        this.youTube = youTube;
    }

    public String getPayInReceiveAmount() {
        return payInReceiveAmount;
    }

    public void setPayInReceiveAmount(String payInReceiveAmount) {
        this.payInReceiveAmount = payInReceiveAmount;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicName() : getEnglishName();
    }

}
