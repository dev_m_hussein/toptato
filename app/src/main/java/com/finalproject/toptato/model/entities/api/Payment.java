package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("Notes")
    @Expose
    private String notes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicName() : getEnglishName();
    }

}
