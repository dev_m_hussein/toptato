package com.finalproject.toptato.model.entities.profile;


public interface ProfileView {

    void startSignUp();

    void startLogIn();

}
