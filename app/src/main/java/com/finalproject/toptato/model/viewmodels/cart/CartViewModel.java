package com.finalproject.toptato.model.viewmodels.cart;

import android.app.Application;

import androidx.annotation.NonNull;

import com.finalproject.toptato.model.viewmodels.BaseViewModel;

public class CartViewModel extends BaseViewModel {

    public CartViewModel(@NonNull Application application) {
        super(application);
    }
}
