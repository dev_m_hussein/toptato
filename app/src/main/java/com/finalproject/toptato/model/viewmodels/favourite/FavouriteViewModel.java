package com.finalproject.toptato.model.viewmodels.favourite;

import android.app.Application;

import com.finalproject.toptato.model.entities.api.LoginResponse;
import com.finalproject.toptato.model.entities.api.MessageResponse;
import com.finalproject.toptato.model.entities.api.Product;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FavouriteViewModel extends BaseViewModel {

    public MutableLiveData<List<Product>> getfavoriteListItemMutable;
    public MutableLiveData<Boolean> isLogin ;

    public FavouriteViewModel(@NonNull Application application) {
        super(application);

        getfavoriteListItemMutable = new MutableLiveData<>();
        isLogin=new MutableLiveData<>();

    }


    public void getFavoritesListItems() {


      if (getUserData().getUserLogin() != null) {

           LoginResponse login = getUserData().getUserLogin();


            getRepository().getFavorites(login.getCustomerId())

                .subscribe(new Observer<MessageResponse<Product>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onNext(MessageResponse<Product> response) {

                        if (response.isSuccess() && response.getDataList() != null) {

                            List<Product> favoriteLists = response.getDataList();
                            getfavoriteListItemMutable.postValue(favoriteLists);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        setIsLoading(false);
                    }
                });
                     isLogin.setValue(true);
              } else
                   isLogin.setValue(false);
                {

                       }
}

}
