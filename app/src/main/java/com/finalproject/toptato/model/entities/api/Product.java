package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {

    @SerializedName(value = "id", alternate = {"Id", "ItemId"})
    @Expose
    private Integer id;
    @SerializedName("RefNumber")
    @Expose
    private String refNumber;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("DescAR")
    @Expose
    private String descArabic;
    @SerializedName("DescEN")
    @Expose
    private String descEnglish;
    @SerializedName("ItemOrder")
    @Expose
    private Integer itemOrder;
    @SerializedName("ArabicDetails")
    @Expose
    private String arabicDetails;
    @SerializedName("EnglishDetails")
    @Expose
    private String englishDetails;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;
    @SerializedName("CategoryVideo")
    @Expose
    private String categoryVideo;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("IsNewItem")
    @Expose
    private Boolean isNewItem;
    @SerializedName("UnitPrice")
    @Expose
    private Integer unitPrice;
    @SerializedName("ReviewStars")
    @Expose
    private Double reviewStars;
    @SerializedName("IsPromotion")
    @Expose
    private Boolean isPromotion;
    @SerializedName("PromotionUnitPrice")
    @Expose
    private Integer promotionUnitPrice;
    @SerializedName("RecieveDetails")
    @Expose
    private List<Object> recieveDetails = null;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("CategoryArabicName")
    @Expose
    private String categoryArabicName;
    @SerializedName("CategoryEnglishName")
    @Expose
    private String categoryEnglishName;
    @SerializedName("BrandId")
    @Expose
    private Integer brandId;
    @SerializedName("BrandArabicName")
    @Expose
    private String brandArabicName;
    @SerializedName("BrandEnglishName")
    @Expose
    private String brandEnglishName;
    @SerializedName("ColorList")
    @Expose
    private List<Color> colorList = null;
    @SerializedName("SizeList")
    @Expose
    private List<Size> sizeList = null;
    @SerializedName("ImagesList")
    @Expose
    private List<Image> imagesList = null;
    @SerializedName("MatrixList")
    @Expose
    private List<Size> matrixList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getDescArabic() {
        return descArabic;
    }

    public void setDescArabic(String descArabic) {
        this.descArabic = descArabic;
    }

    public String getDescEnglish() {
        return descEnglish;
    }

    public void setDescEnglish(String descEnglish) {
        this.descEnglish = descEnglish;
    }

    public Integer getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(Integer itemOrder) {
        this.itemOrder = itemOrder;
    }

    public String getArabicDetails() {
        return arabicDetails;
    }

    public void setArabicDetails(String arabicDetails) {
        this.arabicDetails = arabicDetails;
    }

    public String getEnglishDetails() {
        return englishDetails;
    }

    public void setEnglishDetails(String englishDetails) {
        this.englishDetails = englishDetails;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Boolean isNewItem() {
        return isNewItem;
    }

    public void setNewItem(Boolean newItem) {
        isNewItem = newItem;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(Double reviewStars) {
        this.reviewStars = reviewStars;
    }

    public Boolean isPromotion() {
        return isPromotion;
    }

    public void setPromotion(Boolean promotion) {
        isPromotion = promotion;
    }

    public Integer getPromotionUnitPrice() {
        return promotionUnitPrice;
    }

    public void setPromotionUnitPrice(Integer promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
    }

    public List<Object> getRecieveDetails() {
        return recieveDetails;
    }

    public void setRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryArabicName() {
        return categoryArabicName;
    }

    public void setCategoryArabicName(String categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
    }

    public String getCategoryEnglishName() {
        return categoryEnglishName;
    }

    public void setCategoryEnglishName(String categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandArabicName() {
        return brandArabicName;
    }

    public void setBrandArabicName(String brandArabicName) {
        this.brandArabicName = brandArabicName;
    }

    public String getBrandEnglishName() {
        return brandEnglishName;
    }

    public void setBrandEnglishName(String brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
    }

    public List<Color> getColorList() {
        return colorList;
    }

    public void setColorList(List<Color> colorList) {
        this.colorList = colorList;
    }

    public List<Size> getSizeList() {
        return sizeList;
    }

    public void setSizeList(List<Size> sizeList) {
        this.sizeList = sizeList;
    }

    public List<Image> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<Image> imagesList) {
        this.imagesList = imagesList;
    }

    public List<Size> getMatrixList() {
        return matrixList;
    }

    public void setMatrixList(List<Size> matrixList) {
        this.matrixList = matrixList;
    }

    public String getName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicName() : getEnglishName();
    }

    public String getDesc(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getDescArabic() : getDescEnglish();
    }

    public String getDetails(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicDetails() : getEnglishDetails();
    }

    public String getCategoryName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getCategoryArabicName() : getCategoryEnglishName();
    }

    public String getBrandName(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getBrandArabicName() : getBrandEnglishName();
    }

}
