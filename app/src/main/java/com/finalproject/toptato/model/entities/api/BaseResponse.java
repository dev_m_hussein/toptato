package com.finalproject.toptato.model.entities.api;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BaseResponse<T> implements Serializable {

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("error_description")
    @Expose
    private String errorDescription;

    @SerializedName(value = "dataList", alternate = {"ItemList", "SizeList", "FavoriteList",
            "ColorList", "BestOffers", "SiteSliderList", "ItemCommment", "BranchesLst",
            "BestSeller", "CategoryList"})
    @Expose
    private List<T> dataList;

    @SerializedName(value = "data", alternate = {"Data"})
    @Expose
    private T data;

    public boolean isSuccess() {
        return (!TextUtils.isEmpty(success)) &&
                (success.equalsIgnoreCase("ok") || success.equalsIgnoreCase("true"));
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
