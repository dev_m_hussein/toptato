package com.finalproject.toptato.model.entities.api.itemdetailsModel;

import com.google.gson.annotations.SerializedName;

public class ColorsItem{

	@SerializedName("ColorEnglishName")
	private String colorEnglishName;

	@SerializedName("ColorCode")
	private String colorCode;

	@SerializedName("AvailableQuantity")
	private int availableQuantity;

	@SerializedName("ColorArabicName")
	private String colorArabicName;

	@SerializedName("ColorId")
	private int colorId;

	public void setColorEnglishName(String colorEnglishName){
		this.colorEnglishName = colorEnglishName;
	}

	public String getColorEnglishName(){
		return colorEnglishName;
	}

	public void setColorCode(String colorCode){
		this.colorCode = colorCode;
	}

	public String getColorCode(){
		return colorCode;
	}

	public void setAvailableQuantity(int availableQuantity){
		this.availableQuantity = availableQuantity;
	}

	public int getAvailableQuantity(){
		return availableQuantity;
	}

	public void setColorArabicName(String colorArabicName){
		this.colorArabicName = colorArabicName;
	}

	public String getColorArabicName(){
		return colorArabicName;
	}

	public void setColorId(int colorId){
		this.colorId = colorId;
	}

	public int getColorId(){
		return colorId;
	}

	@Override
 	public String toString(){
		return 
			"ColorsItem{" + 
			"colorEnglishName = '" + colorEnglishName + '\'' + 
			",colorCode = '" + colorCode + '\'' + 
			",availableQuantity = '" + availableQuantity + '\'' + 
			",colorArabicName = '" + colorArabicName + '\'' + 
			",colorId = '" + colorId + '\'' + 
			"}";
		}
}