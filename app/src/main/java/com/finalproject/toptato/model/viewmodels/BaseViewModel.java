package com.finalproject.toptato.model.viewmodels;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.finalproject.toptato.repository.Repository;

public abstract class BaseViewModel extends AndroidViewModel {

    private static UserData userData;
    private static Repository repository;
    private final MutableLiveData<Boolean> mIsLoading = new MutableLiveData<>();

    public BaseViewModel(@NonNull Application application) {
        super(application);
        userData = UserData.getInstance(application);
        repository = Repository.getInstance(application);
    }

    public static UserData getUserData() {
        return userData;
    }

    public static Repository getRepository() {
        return repository;
    }

    public LiveData<Boolean> isLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.setValue(isLoading);
    }
}
