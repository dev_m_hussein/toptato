package com.finalproject.toptato.model.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.model.entities.api.ScreenShootBitmap;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.ItemListItem;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;
import com.finalproject.toptato.model.utils.SingleLiveEvent;
import com.finalproject.toptato.model.utils.createScreenShootUtils.CreatBitmapCanvasUtils;
import com.finalproject.toptato.model.utils.createScreenShootUtils.ScreenShootUtils;
import com.finalproject.toptato.model.utils.createScreenShootUtils.ShareBitmapUtil;
import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.Gson;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;


public class ItemdetailsViewModel extends BaseViewModel {
    public MutableLiveData<Response> itemdetailsMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<File> fileShareLiveData = new SingleLiveEvent<>();

    private Activity activity;
    private ScreenShootBitmap screenShootBitmap;

    public ItemdetailsViewModel(@NonNull Application application) {
        super(application);
    }


    public void getitemdetails(int itemId) {
        getRepository().getallitemdetails(itemId).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                itemdetailsMutableLiveData.setValue(response.body());

                //TODO : check here
                Log.d("aseel", new Gson().toJson(response.body() ));
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.d("aseel","errrorr");

            }
        });
    }


    /**
     * when click user share button convert the detail of item to Object ScreenShootBitmap and sent it to method shareScreenshoot
     *
     * @param selectedSize  the selected size from user
     * @param selectedColor the selected color from user
     * @param activity      the activity
     *                      <p>
     *                      created by mahmoud saad
     */
    public void onClickShareButton(int selectedSize, int selectedColor, Activity activity) {
        if (itemdetailsMutableLiveData.getValue() == null || itemdetailsMutableLiveData.getValue().getItemList().get(0) == null)
            return;
        ItemListItem item = itemdetailsMutableLiveData.getValue().getItemList().get(0);

        this.activity = activity;

        boolean isDirectionRTL = UserData.getInstance(getApplication()).getLocalization() == Localization.ARABIC_VALUE;
        Disposable observable = Observable.fromCallable(() ->
                ScreenShootUtils.prepareScreenShootDetail(item, isDirectionRTL, selectedSize, selectedColor))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::shareScreenShoot);


    }

    /**
     * create bitmap and share of bitmap
     *
     * @param screenShootBitmap all detail of item that selected by user
     *                          <p>
     *                          <p>
     *                          created by mahmoud saad
     */
    public void shareScreenShoot(ScreenShootBitmap screenShootBitmap) {

        Bitmap bitmap = createBitmapfromScreenDetail(screenShootBitmap);

        shareBitamp(bitmap);


    }

    /**
     * convert object screenshootBitmap to Bitmap
     *
     * @param screenShootBitmap object hold detail all of item
     * @return bitmap
     * <p>
     * created by mahmoud saad
     */
    private Bitmap createBitmapfromScreenDetail(ScreenShootBitmap screenShootBitmap) {
        CreatBitmapCanvasUtils creatBitmapCanvasUtils = new CreatBitmapCanvasUtils(
                activity,
                screenShootBitmap);

        return creatBitmapCanvasUtils.createScreenBitmap();
    }

    /**
     * save the bitmap in provider and sent the location of bitmap to activity to share it
     *
     * @param bitmap the bitmap of item after created
     *               <p>
     *               created by mahmoud saad
     */
    private void shareBitamp(Bitmap bitmap) {

        ShareBitmapUtil.createFileDirObservable(getApplication(), bitmap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<File>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(File file) {
                        fileShareLiveData.setValue(file);
                    }


                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
