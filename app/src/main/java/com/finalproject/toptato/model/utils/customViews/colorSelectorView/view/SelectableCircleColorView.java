package com.finalproject.toptato.model.utils.customViews.colorSelectorView.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;

import com.finalproject.toptato.model.utils.customViews.colorSelectorView.anim.AnimatedInteger;

/**
 * Created by Osman Ibrahiem
 */
public class SelectableCircleColorView extends CircleColorView {

    private static final float MAX_SCALE = 1;
    private static final float MIN_SCALE = 0.8f;

    private AnimatedInteger size;
    private float scale = MAX_SCALE;

    public SelectableCircleColorView(Context context) {
        super(context);
    }

    public SelectableCircleColorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectableCircleColorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void init() {
        super.init();
        size = new AnimatedInteger(0);
    }

    /**
     * Specify whether the color is currently "selected".
     * <p>
     * This animates the color circle's scale between 100%
     * and 80% of the size of the view depending on whether
     * it is selected or unselected, respectively.
     * <p>
     * If this method is not called, the view defaults to the
     * "selected" state.
     *
     * @param selected Whether the view is selected.
     */
    public void setSelected(boolean selected) {
        scale = selected ? MAX_SCALE : MIN_SCALE;
        outlinePaint.setColor(selected ? Color.BLACK : Color.TRANSPARENT);

        postInvalidate();
    }

    @Override
    public void draw(Canvas canvas) {
        int size = Math.min(getWidth(), getHeight());
        int target = (int) (size * scale);
        if (target != this.size.getTarget())
            this.size.to(target);

        this.size.next(true);

        float scale = (float) this.size.val() / size;
        canvas.scale(scale, scale, getWidth() / 2, getHeight() / 2);
        super.draw(canvas);

        if (!this.size.isTarget())
            postInvalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int size = getMeasuredWidth();
        setMeasuredDimension(size, size);
    }

}