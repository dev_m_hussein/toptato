package com.finalproject.toptato.model.entities.api;

import android.content.Context;

import com.finalproject.toptato.model.utils.sharedTool.Localization;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageResponse<T> extends BaseResponse<T> {

    @SerializedName(value = "arabicMsg", alternate = {"arabicMessage", "Message"})
    @Expose
    private String arabicMessage;
    @SerializedName(value = "englishMsg", alternate = {"englishMessage"})
    @Expose
    private String englishMessage;
    @SerializedName("ActivationType")
    @Expose
    private Integer activationType;
    @SerializedName("OrderId")
    @Expose
    private Integer orderId;

    public String getMessage(Context context) {
        return (UserData.getInstance(context).getLocalization() == Localization.ARABIC_VALUE) ? getArabicMessage() : getEnglishMessage();
    }

    public String getArabicMessage() {
        return arabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        this.arabicMessage = arabicMessage;
    }

    public String getEnglishMessage() {
        return englishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        this.englishMessage = englishMessage;
    }

    public Integer getActivationType() {
        return activationType;
    }

    public void setActivationType(Integer activationType) {
        this.activationType = activationType;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
