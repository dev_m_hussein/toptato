package com.finalproject.toptato.model.viewmodels.signup;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.MessageResponse;
import com.finalproject.toptato.model.utils.SingleLiveEvent;
import com.finalproject.toptato.model.utils.validation.EmailValid;
import com.finalproject.toptato.model.utils.validation.EmptyValid;
import com.finalproject.toptato.model.utils.validation.PasswordValid;
import com.finalproject.toptato.model.utils.validation.PhoneNumberValid;
import com.finalproject.toptato.model.viewmodels.BaseViewModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by mahmoud saad
 */
public class SignUpViewModel extends BaseViewModel {
    private final String TAG = getClass().getSimpleName();

    private MutableLiveData<String> dataOfBirthMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<String> firstName = new MutableLiveData<>();
    private MutableLiveData<String> lastName = new MutableLiveData<>();
    private MutableLiveData<String> email = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<String> phoneNumber = new MutableLiveData<>();
    private MutableLiveData<String> address = new MutableLiveData<>();
    private MutableLiveData<String> street = new MutableLiveData<>();
    private MutableLiveData<Boolean> recieveNewsLetter = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> genderMale = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> genderFemale = new MutableLiveData<>(false);

    private MutableLiveData<String> displayMessage = new MutableLiveData<>();
    private MutableLiveData<Boolean> genderError = new MutableLiveData<>();
    private MutableLiveData<Boolean> startNewActivity = new SingleLiveEvent<>();

    public SignUpViewModel(@NonNull Application application) {
        super(application);
    }


    public MutableLiveData<String> getDataOfBirthMutableLiveData() {
        return dataOfBirthMutableLiveData;
    }

    public MutableLiveData<Boolean> getRecieveNewsLetter() {
        return recieveNewsLetter;
    }

    public MutableLiveData<Boolean> getGenderMale() {
        return genderMale;
    }

    public MutableLiveData<Boolean> getGenderFemale() {
        return genderFemale;
    }

    public MutableLiveData<Boolean> getStartNewActivity() {
        return startNewActivity;
    }

    public MutableLiveData<String> getDisplayMessage() {
        return displayMessage;
    }

    public MutableLiveData<String> getFirstName() {
        return firstName;
    }

    public MutableLiveData<String> getAddress() {
        return address;
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public MutableLiveData<String> getLastName() {
        return lastName;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }

    public MutableLiveData<String> getStreet() {
        return street;
    }

    public MutableLiveData<Boolean> getGenderError() {
        return genderError;
    }



    public boolean checkGenderEmpty() {
        if (genderMale.getValue() == null || genderFemale.getValue() == null) return false;

        return genderFemale.getValue() || genderMale.getValue();


    }


    public boolean isInputValid() {
        boolean isEmpty = false;
        EmptyValid emptyValid=new EmptyValid();
        if (!checkGenderEmpty()) {
            genderError.setValue(true);
            isEmpty = true;
        }
        if (!emptyValid.valid(firstName.getValue())) {
            firstName.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(lastName.getValue())) {
            lastName.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(email.getValue())) {
            email.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(password.getValue())) {
            password.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(phoneNumber.getValue())) {
            phoneNumber.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(address.getValue())) {
            address.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(street.getValue())) {
            street.setValue("");
            isEmpty = true;
        }
        if (!emptyValid.valid(dataOfBirthMutableLiveData.getValue())) {
            dataOfBirthMutableLiveData.setValue("");
            isEmpty = true;
        }

        return !isEmpty && new EmailValid().valid(email.getValue()) &&
                new PasswordValid().valid(password.getValue()) &&
                new PhoneNumberValid().valid(phoneNumber.getValue());
    }

    public void onButtonRegisterClick() {
        setIsLoading(true);
        Log.i(TAG, "onButtonRegisterClick: progress bar is show ");


        if (!isInputValid()) {
            setIsLoading(false);

            return;
        }


        Map<String, Object> queryMap = new HashMap<>();

        queryMap.put("FirstName", firstName.getValue());
        queryMap.put("SecondName", lastName.getValue());
        queryMap.put("ImagePath", "man.png");
        queryMap.put("Mobile", phoneNumber.getValue());
        queryMap.put("E_Mail", email.getValue());
        queryMap.put("LogInPassword", password.getValue());
        queryMap.put("BirthDay", dataOfBirthMutableLiveData.getValue());

        Log.i(TAG, "onButtonRegisterClick: gender is " + genderMale.getValue());

        queryMap.put("Gender", genderMale.getValue());
        queryMap.put("RecieveNewsLetter", recieveNewsLetter.getValue());
        queryMap.put("CountryId", "1");
        queryMap.put("CityId", "1");
        queryMap.put("DistricId", "1");
        queryMap.put("StreetName", street.getValue());
        queryMap.put("Address", address.getValue());

        getRepository()
                .userRegister(queryMap)
                .subscribe(new Observer<MessageResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "onSubscribe: ");


                    }

                    @Override
                    public void onNext(MessageResponse register) {
                        setIsLoading(false);

                        if (register == null) return;
                        if (register.getError() != null) {
                            displayMessage.setValue(getApplication().getString(R.string.message_expired_time));
                        } else if (register.getSuccess() != null) {

                            switch (register.getSuccess()) {
                                case "error":
                                    displayMessage.setValue(register.getMessage(getApplication()));
                                    startNewActivity.setValue(true);
                                    break;
                                default:
                                    displayMessage.setValue("try again");
                                    break;
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                        Log.i(TAG, "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        setIsLoading(false);
                        Log.i(TAG, "onComplete: ");
                    }
                });
    }


}
