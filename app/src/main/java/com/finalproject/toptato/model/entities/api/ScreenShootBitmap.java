package com.finalproject.toptato.model.entities.api;

import android.graphics.Bitmap;

import java.util.List;

public class ScreenShootBitmap {
    private List<Bitmap> imageBitmapList;
    private List<Integer> colorValueList;
    private List<String> sizeItemList, colorNameList;
    private String descriptionItemText, codeNumberItem;
    private boolean directionRTL;
    private int indexSelectedColor, indexSelectedSize;

    public ScreenShootBitmap() {
    }

    public ScreenShootBitmap(List<Bitmap> imageBitmapList, List<Integer> colorValueList, List<String> sizeItemList, List<String> colorNameList, String descriptionItemText, String codeNumberItem, boolean directionRTL, int indexSelectedColor, int indexSelectedSize) {
        this.imageBitmapList = imageBitmapList;
        this.colorValueList = colorValueList;
        this.sizeItemList = sizeItemList;
        this.colorNameList = colorNameList;
        this.descriptionItemText = descriptionItemText;
        this.codeNumberItem = codeNumberItem;
        this.directionRTL = directionRTL;
        this.indexSelectedColor = indexSelectedColor;
        this.indexSelectedSize = indexSelectedSize;
    }

    public int getIndexSelectedColor() {
        return indexSelectedColor;
    }

    public void setIndexSelectedColor(int indexSelectedColor) {
        this.indexSelectedColor = indexSelectedColor;
    }

    public int getIndexSelectedSize() {
        return indexSelectedSize;
    }

    public void setIndexSelectedSize(int indexSelectedSize) {
        this.indexSelectedSize = indexSelectedSize;
    }

    public String getCodeNumberItem() {
        return codeNumberItem;
    }

    public void setCodeNumberItem(String codeNumberItem) {
        this.codeNumberItem = codeNumberItem;
    }

    public boolean isDirectionRTL() {
        return directionRTL;
    }

    public void setDirectionRTL(boolean directionRTL) {
        this.directionRTL = directionRTL;
    }

    public List<Bitmap> getImageBitmapList() {
        return imageBitmapList;
    }

    public void setImageBitmapList(List<Bitmap> imageBitmapList) {
        this.imageBitmapList = imageBitmapList;
    }

    public List<Integer> getColorValueList() {
        return colorValueList;
    }

    public void setColorValueList(List<Integer> colorValueList) {
        this.colorValueList = colorValueList;
    }

    public List<String> getSizeItemList() {
        return sizeItemList;
    }

    public void setSizeItemList(List<String> sizeItemList) {
        this.sizeItemList = sizeItemList;
    }

    public List<String> getColorNameList() {
        return colorNameList;
    }

    public void setColorNameList(List<String> colorNameList) {
        this.colorNameList = colorNameList;
    }

    public String getDescriptionItemText() {
        return descriptionItemText;
    }

    public void setDescriptionItemText(String descriptionItemText) {
        this.descriptionItemText = descriptionItemText;
    }
}
