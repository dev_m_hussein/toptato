package com.finalproject.toptato.repository.retrofit;

import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.entities.api.Category;
import com.finalproject.toptato.model.entities.api.Color;
import com.finalproject.toptato.model.entities.api.LoginResponse;
import com.finalproject.toptato.model.entities.api.MessageResponse;
import com.finalproject.toptato.model.entities.api.Product;
import com.finalproject.toptato.model.entities.api.ResendActivation;
import com.finalproject.toptato.model.entities.api.Size;
import com.finalproject.toptato.model.entities.api.Token;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created By Osman Ibrahiem
 */
public interface ApiService {

    @POST(ApiConstants.END_POINT_TOKEN)
    @FormUrlEncoded
    Observable<Token> getToken(
            @Field(ApiConstants.TOKEN_PARAM_USERNAME) String userName,
            @Field(ApiConstants.TOKEN_PARAM_PASSWORD) String password,
            @Field(ApiConstants.TOKEN_PARAM_GRANT_TYPE) String grantType,
            @Field(ApiConstants.TOKEN_PARAM_DEVICE_NAME) String deviceName
    );

    @POST(ApiConstants.ENT_POINT_REGISTER)
    Observable<MessageResponse> userRegister(
            @QueryMap Map<String, Object> queryMap
    );

    @POST(ApiConstants.END_POINT_LOGIN)
    Observable<LoginResponse> userLogin(
            @QueryMap Map<String, Object> queryMap
    );

    @POST(ApiConstants.END_POINT_RESEND_ACTIVATION)
    Observable<ResendActivation> resendActivationCode(
            @QueryMap Map<String, Object> queryMap
    );

    @GET(ApiConstants.END_POINT_BRANCHES)
    Observable<MessageResponse<Branch>> getBranches();

    @POST(ApiConstants.END_POINT_FAVORITES)
    Observable<MessageResponse<Product>> getFavoritesItems(
            @Query(ApiConstants.FAVORITES_PARAM_CUSTOMER_ID) int id
    );

    @POST(ApiConstants.END_POINT_CATEGORIES)
    Observable<MessageResponse<Category>> getCategories();

    @POST(ApiConstants.END_POINT_COLORS)
    Observable<MessageResponse<Color>> getColors();

    @POST(ApiConstants.END_POINT_SIZES)
    Observable<MessageResponse<Size>> getSizes();

    @POST(ApiConstants.END_POINT_FILTER_ITEMS)
    Observable<MessageResponse<Product>> filterProducts(
            @QueryMap Map<String, Object> queryMap
    );


    @POST(ApiConstants.END_POINT_ITEM_DETAILS)
    //TODO : check here
        Call<Response> getallitemdetails(@Query("ItemId") int id);



}
