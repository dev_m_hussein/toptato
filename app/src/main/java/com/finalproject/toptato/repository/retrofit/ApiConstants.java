package com.finalproject.toptato.repository.retrofit;

public final class ApiConstants {

    public static final String BASE_URL = "http://smartpan.com.sa:9001/";

    /**
     * Api end_points
     */
    public static final String END_POINT_TOKEN = "Token";
    public static final String END_POINT_BRANCHES = "TrainingAPI/GetBranches";
    public static final String ENT_POINT_REGISTER = "TrainingAPI/Register";
    public static final String END_POINT_LOGIN = "TrainingAPI/Login";
    public static final String END_POINT_RESEND_ACTIVATION = "TrainingAPI/ResendActivation";
    public static final String END_POINT_FAVORITES = "TrainingAPI/GetFavoriteItems";
    public static final String END_POINT_CATEGORIES = "TrainingAPI/GetCategories";
    public static final String END_POINT_COLORS = "TrainingAPI/GetColors";
    public static final String END_POINT_SIZES = "TrainingAPI/GetSizes";
    public static final String END_POINT_FILTER_ITEMS = "TrainingAPI/FilterItems";
    public static final String END_POINT_ITEM_DETAILS = "TrainingAPI/GetItemDetails";



    /**
     * End_Point parameters
     */
    public static final String TOKEN_PARAM_AUTHORIZATION = "Authorization";
    public static final String TOKEN_PARAM_BEARER = "Bearer ";

    public static final String TOKEN_PARAM_USERNAME = "username";
    public static final String TOKEN_PARAM_PASSWORD = "password";
    public static final String TOKEN_PARAM_GRANT_TYPE = "grant_type";
    public static final String TOKEN_PARAM_DEVICE_NAME = "deviceName";

    public static final String FAVORITES_PARAM_CUSTOMER_ID = "CustomerId";

    public static final String FILTER_PARAM_CATEGORY_ID = "categoryId";
    public static final String FILTER_PARAM_COLOR_ID = "colorId";
    public static final String FILTER_PARAM_SIZE_ID = "sizeId";
    public static final String FILTER_PARAM_SORT_BY = "sortBy";
}
