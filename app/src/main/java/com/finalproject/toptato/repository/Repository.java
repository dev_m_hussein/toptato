package com.finalproject.toptato.repository;

import android.content.Context;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.entities.api.Category;
import com.finalproject.toptato.model.entities.api.Color;
import com.finalproject.toptato.model.entities.api.LoginResponse;
import com.finalproject.toptato.model.entities.api.MessageResponse;
import com.finalproject.toptato.model.entities.api.Product;
import com.finalproject.toptato.model.entities.api.ResendActivation;
import com.finalproject.toptato.model.entities.api.Size;
import com.finalproject.toptato.model.entities.api.Token;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;
import com.finalproject.toptato.repository.retrofit.ApiConfig;
import com.finalproject.toptato.repository.retrofit.ApiService;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;

public final class Repository {

    private static Repository instance;
    private static ApiService service;
    private Context context;

    private Repository(Context context) {
        this.context = context;
        service = ApiConfig.getInstance(context).getService();

    }

    public static Repository getInstance(Context context) {
        if (instance == null)
            instance = new Repository(context);
        return instance;
    }

    public Observable<Token> getToken() {
        String username = context.getString(R.string.token_username);
        String password = context.getString(R.string.token_password);
        String grantType = context.getString(R.string.token_grant_type);
        String deviceName = context.getString(R.string.token_deviceName);

        return service
                .getToken(username, password, grantType, deviceName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<LoginResponse> userLogin(Map<String, Object> queryMap) {
        return service
                .userLogin(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<ResendActivation> resendActivation(Map<String, Object> queryMap) {
        return service
                .resendActivationCode(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<MessageResponse> userRegister(Map<String, Object> queryMap) {
        return service
                .userRegister(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<MessageResponse<Branch>> getBranches() {
        return service
                .getBranches()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MessageResponse<Product>> getFavorites(int customerId) {
        return service
                .getFavoritesItems(customerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<MessageResponse<Category>> getCategories() {
        return service
                .getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<MessageResponse<Color>> getColors() {
        return service
                .getColors()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<MessageResponse<Size>> getSize() {
        return service
                .getSizes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<MessageResponse<Product>> filterProducts(Map<String, Object> queryMap) {
        return service
                .filterProducts(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }
    public Call<Response> getallitemdetails(int itemId){
        //TODO : check here
        return service.getallitemdetails(itemId);
    }



}
