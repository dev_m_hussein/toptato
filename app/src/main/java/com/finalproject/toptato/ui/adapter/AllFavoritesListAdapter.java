package com.finalproject.toptato.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.FavoriteGridItemBinding;
import com.finalproject.toptato.databinding.RowItemCategoryBinding;
import com.finalproject.toptato.model.entities.api.Product;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


public class AllFavoritesListAdapter  extends RecyclerView.Adapter<AllFavoritesListAdapter.RecyclerViewFavHolder> {

    private List<Product> allFavoriteList ;

    public AllFavoritesListAdapter() {
    }

    public  void setAllFavoriteList(List<Product > allFavoriteList){
        this.allFavoriteList=allFavoriteList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerViewFavHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        FavoriteGridItemBinding binding = FavoriteGridItemBinding.inflate(inflater, parent, false);
        return new RecyclerViewFavHolder(binding);

    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerViewFavHolder holder, int position) {
        Product product =allFavoriteList.get(position);

       holder.ToBind(product);
    }

    @Override
    public int getItemCount() {
        return allFavoriteList ==null ? 0 : allFavoriteList.size();
    }

    public class RecyclerViewFavHolder extends RecyclerView.ViewHolder{

       FavoriteGridItemBinding binding;

        public RecyclerViewFavHolder( FavoriteGridItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        void ToBind(Product product){
           binding.setProduct(product);
//           binding.executePendingBindings();
        }
    }

}
