package com.finalproject.toptato.ui.activities.products;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.ActivityProductsBinding;
import com.finalproject.toptato.model.entities.api.Category;
import com.finalproject.toptato.model.entities.api.Product;
import com.finalproject.toptato.model.viewmodels.categories.ProductsViewModel;
import com.finalproject.toptato.ui.activities.itemdetailsactivity.itemdetailsActivity;
import com.finalproject.toptato.ui.adapter.AllProductsListAdapter;
import com.finalproject.toptato.ui.adapter.ColorSelectorAdapter;

/**
 * Created by Osman Ibrahiem
 */
public class ProductsActivity extends AppCompatActivity {

    private ProductsViewModel viewModel;
    private ActivityProductsBinding binding;
    private ColorSelectorAdapter colorSelectorAdapter;
    private AllProductsListAdapter productsListAdapter;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(ProductsViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_products);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        dialog = new ProgressDialog(this);

        Category category = (Category) getIntent().getSerializableExtra(Category.class.getSimpleName());
        if (category != null) {
            viewModel.setSelectedCategoryID(category.getId());
            setTitle(category.getName(this));
        }

        initDrawer();
        initProducts();
        observersRegisters();
    }

    private void initDrawer() {
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        colorSelectorAdapter = new ColorSelectorAdapter(this)
                .withListener(viewModel::onColorPicked);

        binding.filterColorsRecycler.setHasFixedSize(true);

        RecyclerView.ItemAnimator animator = binding.filterColorsRecycler.getItemAnimator();

        if (animator instanceof SimpleItemAnimator)
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);

        binding.filterColorsRecycler.setLayoutManager(new GridLayoutManager(this, 6));
        binding.filterColorsRecycler.setAdapter(colorSelectorAdapter);

        binding.btnOpenFilter.setOnClickListener(v -> {
            if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                binding.drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                binding.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        binding.btnFilter.setOnClickListener(v -> binding.drawerLayout.closeDrawer(GravityCompat.START));
    }

    private void initProducts() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        binding.allProductsRecycler.setLayoutManager(gridLayoutManager);
        productsListAdapter = new AllProductsListAdapter();
        binding.allProductsRecycler.setAdapter(productsListAdapter);
        productsListAdapter.setOnItemClickListener(this::onProductClicked);

    }

    private void observersRegisters() {
        viewModel.getColors().observe(this, colorSelectorAdapter::setColors);
        viewModel.getSelectedFilterColor().observe(this, colorSelectorAdapter::setSelectedColor);

        viewModel.getProducts().observe(this, productsListAdapter::setProductList);

        viewModel.isLoading().observe(this, this::showLoading);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void onProductClicked(Product product) {
        Intent intent=new Intent(this, itemdetailsActivity.class);
        intent.putExtra(getResources().getString(R.string.item_detail_id_key), product.getId());
        startActivity(intent);

    }

    private void showLoading(boolean isLoading) {
        if (isLoading) {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } else {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
