package com.finalproject.toptato.ui.adapter.ItemdetailsAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;

import java.util.ArrayList;
import java.util.List;


public class sizeAdapter  extends RecyclerView.Adapter<sizeAdapter.viewHolder> {
private Response sizesresponse ;

    @NonNull
    @Override
    public sizeAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.size_item,viewGroup,false);
        return new viewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull sizeAdapter.viewHolder holder, int position) {
            holder.sizebutton.setText(sizesresponse.getItemList().get(0).getMatrixList().get(position).getSizeEnglishName());

    }
    public void setlist(Response sizesresponse){
        this.sizesresponse=sizesresponse;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return sizesresponse == null ? 0 : sizesresponse.getItemList().get(0).getMatrixList().size();
    }

    class viewHolder extends RecyclerView.ViewHolder {

        Button sizebutton;




        public viewHolder(View view) {
            super(view);
            sizebutton = view.findViewById(R.id.sizebtn);




        }
    }
}
