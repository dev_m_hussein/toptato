package com.finalproject.toptato.ui.adapter;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.databinding.BranchRecyclerItemBinding;
import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.interfaces.RecyclerItemClick;
import com.finalproject.toptato.model.utils.GpsProviderHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahmoud saad
 */

public class BranchesListAdapter extends RecyclerView.Adapter<BranchesListAdapter.BranchViewHolder> {

    private List<Branch> mItemList = new ArrayList<>();
    private RecyclerItemClick mRecyclerItemClick;
    private MutableLiveData<Location> mCurrentLocationMutableLiveData;
    private LifecycleOwner mLifecycleOwner;
    private Context mContext;

    public BranchesListAdapter(RecyclerItemClick recyclerItemClick, MutableLiveData<Location> locationMutableLiveData) {
        mRecyclerItemClick = recyclerItemClick;
        mCurrentLocationMutableLiveData = locationMutableLiveData;
        mLifecycleOwner = (LifecycleOwner) recyclerItemClick;
    }

    @NonNull
    @Override
    public BranchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        BranchRecyclerItemBinding itemBinding =
                BranchRecyclerItemBinding.inflate(
                        LayoutInflater.from(parent.getContext()),
                        parent,
                        false);

        return new BranchViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final BranchViewHolder holder, final int position) {
        holder.onBind(mItemList.get(position));
        mCurrentLocationMutableLiveData.observe(mLifecycleOwner, new Observer<Location>() {
            @Override
            public void onChanged(Location location) {
                holder.setDistance(mItemList.get(position), location);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public void setmItemList(List<Branch> branchesLstItem) {
        if (branchesLstItem==null)return;

        clearItemList();

        mItemList.addAll(branchesLstItem);
        notifyDataSetChanged();
    }

   public void clearItemList() {
        mItemList.clear();
        notifyDataSetChanged();
    }


    class BranchViewHolder extends RecyclerView.ViewHolder {
        private BranchRecyclerItemBinding mBinding;

        private BranchViewHolder(@NonNull final BranchRecyclerItemBinding itemView) {
            super(itemView.getRoot());
            mBinding = itemView;
            mBinding.distanceRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int postition = getAdapterPosition();
                    if (mItemList != null && postition != -1)
                        mRecyclerItemClick.onClick(mItemList.get(postition));
                }
            });
        }

        void onBind(final Branch branchesLstItem) {

            if (!TextUtils.isEmpty(branchesLstItem.getAddress())) {
                mBinding.branchAddressTextview.setText(branchesLstItem.getAddress());
                mBinding.branchAddressTextview.setVisibility(View.VISIBLE);
            } else {
                mBinding.branchAddressTextview.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(branchesLstItem.getTele())) {
                mBinding.branchTelephoneTextview.setText(branchesLstItem.getTele());
                mBinding.branchTelephoneTextview.setVisibility(View.VISIBLE);
            } else {
                mBinding.branchTelephoneTextview.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(branchesLstItem.getCountryName(mContext))) {
                mBinding.countryNameTextview.setText(branchesLstItem.getCountryName(mContext));
                mBinding.countryNameTextview.setVisibility(View.VISIBLE);
            } else {
                mBinding.countryNameTextview.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(branchesLstItem.getName(mContext))) {
                mBinding.branchNameTextview.setText(branchesLstItem.getName(mContext));
                mBinding.branchNameTextview.setVisibility(View.VISIBLE);
            } else {
                mBinding.branchNameTextview.setVisibility(View.GONE);
            }
        }

        void setDistance(Branch branchesLstItem, Location location) {
            if (branchesLstItem == null || location == null) return;
            Location distination = new Location("");

            distination.setLongitude(Double.valueOf(branchesLstItem.getXPiont()));
            distination.setLatitude(Double.valueOf(branchesLstItem.getYPiont()));

            mBinding.numberDistanceTextView.setText(GpsProviderHelper.calculateDistance(location, distination));
        }


    }

}

