package com.finalproject.toptato.ui.activities.loginactivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.ActivityLoginBinding;
import com.finalproject.toptato.model.utils.AlertDialogUtils;
import com.finalproject.toptato.model.utils.validation.EmailValid;
import com.finalproject.toptato.model.utils.validation.EmptyValid;
import com.finalproject.toptato.model.utils.validation.MethodsValid;
import com.finalproject.toptato.model.utils.validation.PasswordValid;
import com.finalproject.toptato.model.viewmodels.login.LoginViewModel;
import com.finalproject.toptato.ui.activities.MainActivity;
import com.finalproject.toptato.ui.activities.branchesactivity.BranchesActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;

/**
 * Created by mahmoud saad
 */

public class LoginActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 12;
    private final int EDIT_TEXT_DELAY = 800;
    private ActivityLoginBinding mLoginBinding;
    private LoginViewModel mLoginViewModel;
    private AlertDialog alarmAlertDialog, progressBarAlertDialog;
    private GoogleSignInClient mGoogleSignInClient;

    private Handler emailHandler, passwordHandler,activateHandler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mLoginBinding.setLifecycleOwner(this);

        mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);


        mLoginBinding.setLoginViewModel(mLoginViewModel);


        //check if it first login
        mLoginViewModel.receiveIntent(getIntent());

        initProducts();


    }



    public void initProducts() {

        initSignGoogle();

        observable();

        setSupportActionBar(mLoginBinding.loginToolbar);

        mLoginBinding.loginGoogleButton.setOnClickListener(v -> startGoogleSignIn());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        alarmAlertDialog = AlertDialogUtils.createAlarmAlertDialog(this, getResources().getString(R.string.alert_dialog_title_error), "");
        progressBarAlertDialog = AlertDialogUtils.createProgressBarAlertDialog(this);

        emailHandler =new Handler();
        passwordHandler =new Handler();
        activateHandler=new Handler();

    }

    void initSignGoogle() {
        //google sign in
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(this, MainActivity.class));
        }
        return true;
    }


    public void observable() {
        mLoginViewModel.getEmail().observe(this, s -> MethodsValid.inputValid(
                mLoginBinding.loginEmailEdittext,
                new EmailValid(),
                s,
                emailHandler,
                getResources().getString(R.string.message_email_error),
                EDIT_TEXT_DELAY)
        );
        mLoginViewModel.getPassword().observe(this, s -> MethodsValid.inputValid(
                mLoginBinding.loginPasswordEdittext,
                new PasswordValid(),
                s,
                passwordHandler,
                getResources().getString(R.string.message_password_error),
                EDIT_TEXT_DELAY)
        );

        mLoginViewModel.getActivationCode().observe(this, s -> MethodsValid.inputValid(
                mLoginBinding.loginActivationCodeEdittext,
                new EmptyValid(),
                s,
                activateHandler,
                getResources().getString(R.string.message_not_empty_error),
                EDIT_TEXT_DELAY)
        );

        mLoginViewModel.getDisplayMessage().observe(this,this::showAlarmMessage);

        /*
          move from this activity to app
         */
        mLoginViewModel.getStartNewActivity().observe(this, this::startActivity);

        /*
          show alert dialog progress bar dependence on parameter
         */
        mLoginViewModel.isLoading().observe(this,this::showLoading);

        mLoginViewModel.getFirstLogin().observe(this,this::showViewsActivationCode);
    }


    public void startGoogleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mGoogleSignInClient.signOut();

        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            mLoginViewModel.handleSignInResult(task);
        }
    }

    public void startActivity(boolean d) {
        Intent intent = new Intent(LoginActivity.this, BranchesActivity.class);
        startActivity(intent);
    }
    public void showLoading(boolean show){
        if (progressBarAlertDialog==null)return;

        if (show) {
            progressBarAlertDialog.show();
        } else {
            progressBarAlertDialog.dismiss();
        }
    }

    public void showAlarmMessage(String  message){
        if (message==null|| TextUtils.isEmpty(message))return;

        alarmAlertDialog.setMessage(message);
        alarmAlertDialog.show();
    }



    /**
     * this method appear or disappear all view that belong to activation code or first login
     *
     * @param visible value to determine visible views
     */
    void showViewsActivationCode(boolean visible) {
        if (visible) {
            mLoginBinding.loginActivationCodeRelativeLayout.setVisibility(View.VISIBLE);
            mLoginBinding.loginDivideTextview.setVisibility(View.GONE);
            mLoginBinding.loginGoogleButton.setVisibility(View.GONE);
            mLoginBinding.loginResendActivationTextview.setVisibility(View.VISIBLE);

        } else {
            mLoginBinding.loginActivationCodeRelativeLayout.setVisibility(View.GONE);
            mLoginBinding.loginDivideTextview.setVisibility(View.VISIBLE);
            mLoginBinding.loginGoogleButton.setVisibility(View.VISIBLE);
            mLoginBinding.loginResendActivationTextview.setVisibility(View.GONE);

        }

    }

}
