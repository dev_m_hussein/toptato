package com.finalproject.toptato.ui.adapter.ItemdetailsAdapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;




public class colorsAdapter extends RecyclerView.Adapter<colorsAdapter.viewHolder> {
private Response colorresponse ;

    @NonNull
    @Override
    public colorsAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_color,viewGroup,false);
        return new viewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull colorsAdapter.viewHolder holder, int position) {
       holder.colorimg.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorresponse.getItemList().get(0).getMatrixList().get(0).getColors().get(position).getColorCode())));

        // holder.colorimg.setBackgroundColor(android.graphics.Color.parseColor(colorresponse.getItemList().get(0).getMatrixList().get(0).getColors().get(position).getColorCode()));
holder.colorname.setText(colorresponse.getItemList().get(0).getMatrixList().get(0).getColors().get(position).getColorArabicName());
Log.d("colorsresponsemm",new Gson().toJson(colorresponse.getItemList().get(0).getMatrixList().get(0).getColors().get(0).getColorCode()));
    }
    public void setlistcolor(Response colorresponse){
      this.colorresponse=colorresponse;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return colorresponse == null ? 0 : colorresponse.getItemList().get(0).getMatrixList().get(0).getColors().size();
    }

    class viewHolder extends RecyclerView.ViewHolder {

FloatingActionButton colorimg;
TextView colorname;



        public viewHolder(View view) {
            super(view);
            colorimg = view.findViewById(R.id.color_image);
colorname=view.findViewById(R.id.colorname_txt);



        }
    }
}
