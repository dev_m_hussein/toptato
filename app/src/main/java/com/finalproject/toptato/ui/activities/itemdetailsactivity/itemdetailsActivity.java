package com.finalproject.toptato.ui.activities.itemdetailsactivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;
import com.finalproject.toptato.model.utils.AlertDialogUtils;
import com.finalproject.toptato.model.viewmodels.ItemdetailsViewModel;
import com.finalproject.toptato.ui.adapter.ItemdetailsAdapter.colorsAdapter;
import com.finalproject.toptato.ui.adapter.ItemdetailsAdapter.imageAdapter;
import com.finalproject.toptato.ui.adapter.ItemdetailsAdapter.sizeAdapter;
import com.squareup.picasso.Picasso;

import java.io.File;


public class itemdetailsActivity extends AppCompatActivity {
    ItemdetailsViewModel itemdetailsViewModel;
    RecyclerView.LayoutManager layoutManager,layoutManager2,layoutManager3;
    TextView price,numtxt,brandname,description,checkquantity;
    ImageView catogryimage;
    Button addtocart, plus, minus, numitem, shareButton;

    int count =0;
    private AlertDialog alarmAlertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemdetails);
        price=findViewById(R.id.pricetxt);
        numtxt=findViewById(R.id.numtxt);
        brandname=findViewById(R.id.catogryname_txt);
        description=findViewById(R.id.description);
        catogryimage=findViewById(R.id.item_image);
        checkquantity=findViewById(R.id.quantitiytxt);
        addtocart=findViewById(R.id.addtocart_btn);
        shareButton = findViewById(R.id.sharebtn);
        plus=findViewById(R.id.addbtn);
        minus=findViewById(R.id.minus);
        numitem=findViewById(R.id.numitem);
        itemdetailsViewModel= ViewModelProviders.of(this).get(ItemdetailsViewModel.class);
        RecyclerView recyclerView2=findViewById(R.id.recyclerview2);
        sizeAdapter sizeadapter=new sizeAdapter();
        layoutManager=new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        alarmAlertDialog = AlertDialogUtils.createProgressBarAlertDialog(this);


        recyclerView2.setLayoutManager(layoutManager);
        recyclerView2.setAdapter(sizeadapter);
        RecyclerView recyclerView3=findViewById(R.id.recyclerview3);
        colorsAdapter colorsadapter=new colorsAdapter();
        layoutManager3=new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);

        recyclerView3.setLayoutManager(layoutManager3);
        recyclerView3.setAdapter(colorsadapter);
        RecyclerView recyclerView1=findViewById(R.id.recyclerview1);
        imageAdapter imageadapter=new imageAdapter();
        layoutManager2=new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);


        recyclerView1.setLayoutManager(layoutManager2);
        recyclerView1.setAdapter(imageadapter);

        //prepare intent
        Intent intent=getIntent();
        if (intent.hasExtra(getResources().getString(R.string.item_detail_id_key))){
            itemdetailsViewModel.getitemdetails(intent.getIntExtra(getResources().getString(R.string.item_detail_id_key),0));
        }

        shareButton.setOnClickListener(v -> {
            alarmAlertDialog.show();
            itemdetailsViewModel.onClickShareButton(0, 0, itemdetailsActivity.this);
        });

        //observer of share file live data
        itemdetailsViewModel.fileShareLiveData.observe(this, this::startShareImage);

        itemdetailsViewModel.itemdetailsMutableLiveData.observe(this, new Observer<Response>() {
            @Override
            public void onChanged(Response responses) {
                //TODO : modify here
                sizeadapter.setlist(responses);
                colorsadapter.setlistcolor(responses);


                imageadapter.setlistimage(responses);

                numtxt.setText(responses.getItemList().get(0).getItemCode());
                price.setText(responses.getItemList().get(0).getUnitPrice()+ " ريال " );

                brandname.setText(responses.getItemList().get(0).getArabicName());
                description.setText(responses.getItemList().get(0).getArabicDetails());
                Picasso.get().load(responses.getItemList().get(0).getCategoryImage()).into(catogryimage);
for(int i=0; i<responses.getItemList().size(); i++) {
    if (responses.getItemList().get(0).getMatrixList().get(i).getColors().get(i).getAvailableQuantity() > 0) {
        checkquantity.setText(" متوفر: "+responses.getItemList().get(0).getMatrixList().get(i).getColors().get(i).getAvailableQuantity());

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count ++;
                numitem.setText(count+"");
            }
        });


        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 0) {
                    count = count - 1;
                    numitem.setText(count+"");
                }
            }
});
    } else{
        checkquantity.setText("غير متوفر");
        addtocart.setBackgroundColor(getResources().getColor(R.color.darkgray));
        addtocart.setText("ابلغني");
        addtocart.setTextColor(getResources().getColor(R.color.white));
        Drawable img =getResources().getDrawable( R.drawable.reporticon );
        img.setBounds( 0, 0, 80, 80 );
        addtocart.setCompoundDrawables(img, null, null, null);
        final float scale = getResources().getDisplayMetrics().density;
        int padding_20dp = (int) (30 * scale + 0.5f);
        addtocart.setPadding(padding_20dp,0,0,0);


    }

}
            }
});


    }

    /**
     * start share screen shoot with other app
     *
     * @param file that is file location of image in cache memory
     *             <p>
     *             created by mahmoud
     */
    public void startShareImage(File file) {
        if (file == null) return;

        Intent intent = new Intent(android.content.Intent.ACTION_SEND);

        Uri ur = FileProvider.getUriForFile(itemdetailsActivity.this,
                getResources().getString(R.string.provider_cache_authority),
                file);

        intent.putExtra(Intent.EXTRA_STREAM, ur);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType("image/jpeg");

        alarmAlertDialog.dismiss();
        startActivity(intent);
    }
}
