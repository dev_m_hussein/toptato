package com.finalproject.toptato.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.databinding.RowItemCategoryBinding;
import com.finalproject.toptato.model.entities.api.Category;

import java.util.List;

/**
 * Created by Osman Ibrahiem
 */
public class AllCategoriesListAdapter extends RecyclerView.Adapter<AllCategoriesListAdapter.AllCategoriesListViewHolder> {

    private List<Category> categoryList;
    private OnItemClickListener listener;

    public AllCategoriesListAdapter() {
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AllCategoriesListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowItemCategoryBinding binding = RowItemCategoryBinding.inflate(inflater, parent, false);
        return new AllCategoriesListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AllCategoriesListViewHolder holder, int position) {
        holder.bindTo(categoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return categoryList == null ? 0 : categoryList.size();
    }

    public interface OnItemClickListener {
        void onClick(Category category);
    }

    class AllCategoriesListViewHolder extends RecyclerView.ViewHolder {

        private RowItemCategoryBinding binding;

        AllCategoriesListViewHolder(@NonNull RowItemCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindTo(Category category) {
            binding.setCategory(category);

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) listener.onClick(category);
                }
            });
        }
    }
}
