package com.finalproject.toptato.ui.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.FragmentHomeBinding;
import com.finalproject.toptato.model.utils.sharedTool.UserData;
import com.finalproject.toptato.ui.activities.itemdetailsactivity.itemdetailsActivity;

public class HomeFragment extends Fragment {
    Button btn;

    private FragmentHomeBinding homeBinding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeBinding = FragmentHomeBinding.inflate(inflater, container, false);

        return homeBinding.getRoot();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn= getView().findViewById(R.id.detailsbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), itemdetailsActivity.class);
                startActivity(i);
            }
        });

        initObservables();


    }

    private void initObservables() {

        String access_key = UserData.getInstance(getActivity()).getUserInsertToken();

        Toast.makeText(getContext(), access_key, Toast.LENGTH_LONG).show();


    }
}
