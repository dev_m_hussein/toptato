package com.finalproject.toptato.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.finalproject.toptato.model.viewmodels.splash.SplashViewModel;

public class SplashActivity extends AppCompatActivity {

    private SplashViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(SplashViewModel.class);

        viewModel.setLanguage(this);

        initObservables();
        viewModel.getToken();
    }

    private void initObservables() {

        viewModel.getTokenLiveData().observe(this, tokenResult -> {

            Intent main_activity = new Intent(this, MainActivity.class);
            startActivity(main_activity);
            SplashActivity.this.finish();
        });
    }

}
