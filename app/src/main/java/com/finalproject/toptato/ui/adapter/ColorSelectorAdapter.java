package com.finalproject.toptato.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.Color;
import com.finalproject.toptato.model.utils.customViews.colorSelectorView.view.SelectableCircleColorView;

import java.util.List;

/**
 * Created by Osman Ibrahiem
 */
public class ColorSelectorAdapter extends RecyclerView.Adapter<ColorSelectorAdapter.ViewHolder> {

    private Color selectedColor;
    private List<Color> colors;
    private OnColorPickedListener listener;
    private Context context;

    public ColorSelectorAdapter(Context context) {
        this.context = context;
    }

    public ColorSelectorAdapter withListener(OnColorPickedListener listener) {
        this.listener = listener;
        return this;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
        notifyDataSetChanged();
    }

    public Color getSelectedColor() {
        return this.selectedColor;
    }

    public void setSelectedColor(Color selectedColor) {
        if (selectedColor != null) {
            Log.wtf("mColor", "setSelectedColor" + selectedColor.getCode());
            if (this.selectedColor != null) {
                for (int i = 0; i < colors.size(); i++) {
                    if (isSameColor(colors.get(i)))
                        notifyItemChanged(i);
                }
            }

            this.selectedColor = selectedColor;

            for (int i = 0; i < colors.size(); i++) {
                if (isSameColor(colors.get(i)))
                    notifyItemChanged(i);
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_color_selector, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.colorView.setColor(android.graphics.Color.parseColor(colors.get(position).getCode()));
        holder.colorView.setSelected(isSameColor(colors.get(position)));

        holder.title.setText(colors.get(position).getName(context));

        holder.itemView.setOnClickListener(v -> {
//            setSelectedColor(colors.get(holder.getAdapterPosition()));
            if (listener != null)
                listener.onColorPicked(colors.get(holder.getAdapterPosition()));
        });
    }

    private boolean isSameColor(Color selectedColor) {
        return this.selectedColor != null && this.selectedColor.getColorId().intValue() == selectedColor.getColorId();
    }

    @Override
    public int getItemCount() {
        return colors != null ? colors.size() : 0;
    }

    public interface OnColorPickedListener {
        void onColorPicked(Color color);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private SelectableCircleColorView colorView;
        private AppCompatTextView title;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            colorView = itemView.findViewById(R.id.color);
            title = itemView.findViewById(R.id.title);
        }
    }
}
