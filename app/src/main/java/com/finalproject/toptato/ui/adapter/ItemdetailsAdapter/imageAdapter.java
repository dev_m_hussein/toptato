package com.finalproject.toptato.ui.adapter.ItemdetailsAdapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.itemdetailsModel.Response;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class imageAdapter extends RecyclerView.Adapter<imageAdapter.viewHolder> {
private Response imageresponse ;


    @NonNull
    @Override
    public imageAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_item,viewGroup,false);
        return new viewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull imageAdapter.viewHolder holder, int position) {
Picasso.get().load(imageresponse.getItemList().get(0).getImagesList().get(0).getImagePath()).into(holder.image);
      //  holder.image.setBackground(Drawable.createFromPath(imageresponse.getItemList().get(0).getImagesList().get(0).getImagePath()));

    }
    public void setlistimage( Response imageresponse){
        this.imageresponse=imageresponse;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return imageresponse == null ? 0 : imageresponse.getItemList().get(0).getImagesList().size();
    }
    class viewHolder extends RecyclerView.ViewHolder {

ImageView image;


        public viewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.imageitem);




        }
    }
}
