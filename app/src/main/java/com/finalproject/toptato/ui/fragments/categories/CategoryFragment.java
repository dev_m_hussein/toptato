package com.finalproject.toptato.ui.fragments.categories;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.R;
import com.finalproject.toptato.model.entities.api.Category;
import com.finalproject.toptato.model.viewmodels.categories.ProductsViewModel;
import com.finalproject.toptato.ui.activities.products.ProductsActivity;
import com.finalproject.toptato.ui.adapter.AllCategoriesListAdapter;

import java.util.List;

/**
 * Created by Osman Ibrahiem
 */
public class CategoryFragment extends Fragment {

    private ProductsViewModel categoryViewModel;
    private RecyclerView recyclerView;

    private ProgressDialog dialog;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        categoryViewModel = ViewModelProviders.of(this).get(ProductsViewModel.class);

        View view = inflater.inflate(R.layout.fragment_category, container, false);

        recyclerView = view.findViewById(R.id.all_category);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        dialog = new ProgressDialog(getActivity());

        observersRegisters();
        return view;
    }

    private void observersRegisters() {

        final AllCategoriesListAdapter adapter = new AllCategoriesListAdapter();

        adapter.setOnItemClickListener(new AllCategoriesListAdapter.OnItemClickListener() {
            @Override
            public void onClick(Category category) {
                CategoryFragment.this.onProductClicked(category);
            }
        });

         recyclerView.setAdapter(adapter);
         categoryViewModel.getCategoriesList().observe(this, new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categoryList) {
                adapter.setCategoryList(categoryList);
            }
        });
        categoryViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                CategoryFragment.this.showLoading(isLoading);
            }
        });
    }

    private void onProductClicked(Category category) {
        Intent intent = new Intent(getActivity(), ProductsActivity.class);
        intent.putExtra(Category.class.getSimpleName(), category);
        startActivity(intent);
    }

    private void showLoading(boolean isLoading) {
        if (isLoading) {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } else {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
