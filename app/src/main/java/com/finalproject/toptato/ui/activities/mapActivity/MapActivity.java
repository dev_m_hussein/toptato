package com.finalproject.toptato.ui.activities.mapActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.ActivityMapBinding;
import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.utils.AlertDialogUtils;
import com.finalproject.toptato.model.utils.GpsProviderHelper;
import com.finalproject.toptato.model.viewmodels.map.MapViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

/**
 * Created by mahmoud saad
 */

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    private final String TAG = getClass().getSimpleName();
    private GoogleMap mGoogleMap;
    private ActivityMapBinding mMapBinding;
    private MapViewModel mMapViewModel;
    private Intent receiveIntent;
    private AlertDialog locationAlertDialog;
    private boolean slideToggle ;

    private GpsProviderHelper mGpsProvider = new GpsProviderHelper() {
        @Override
        public Activity getActivity() {
            return MapActivity.this;
        }

        @Override
        public void onNeedPermission() {
            super.onNeedPermission();
            ActivityCompat.requestPermissions(MapActivity.this
                    , new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION
                            , Manifest.permission.ACCESS_COARSE_LOCATION}
                    , PERMISSION_LOCATION_REQUEST_CODE
            );
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMapBinding = DataBindingUtil.setContentView(this, R.layout.activity_map);
        mMapBinding.setLifecycleOwner(this);

        mMapViewModel = ViewModelProviders.of(this).get(MapViewModel.class);

        mMapBinding.setMapViewModel(mMapViewModel);


        //get all branches are selected from intent and current location if have
        receiveIntent = getIntent();



        initProducts();

        //init All Observables
        observables();

        initalMap();

    }


    private void initProducts() {

        mMapViewModel.prepareLocationIntent(receiveIntent);

        //make slide is disappear
        mMapBinding.slideMainRelative.setVisibility(View.GONE);

        mMapBinding.slideRelativeLayout.setOnClickListener(this::showSlide);

        locationAlertDialog = AlertDialogUtils.createAlarmAlertDialog(
                MapActivity.this,
                getResources().getString(R.string.alert_dialog_title_warning),
                getResources().getString(R.string.message_warning_open_gps)
        );
    }

    private void observables() {


        mMapViewModel.getListLatLangLiveData().observe(this,this::addBranchToMap);

        /*
          appear the slide and set data of branch to slide
         */
        mMapViewModel.getSelectedBranchMutableLiveData().observe(this,this::showDataOnSlide);

        mMapViewModel.getPrepareCurrentLocationMutableLiveData().observe(this, aBoolean -> {
                if (aBoolean)
                    getLocation();
            });

        mMapViewModel.getStartNavigationMapMutableLiveData().observe(this,this::startNavigation);

    }


    private void initalMap() {
        Log.i(TAG, "initalMap: ");
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view);
        if (supportMapFragment != null)
            supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(TAG, "onMapReady: ");
        mGoogleMap = googleMap;

        mGoogleMap.setOnMapLoadedCallback(() -> {
            if (receiveIntent != null) {
                mMapViewModel.prepareBranchesIntent(receiveIntent);

            }
        });

        mGoogleMap.setOnMarkerClickListener(this::markerClick);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GpsProviderHelper.PERMISSION_LOCATION_REQUEST_CODE) {

            if (!GpsProviderHelper.hasPermission(getApplicationContext())) {
                locationAlertDialog.show();
            } else {
                getLocation();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("TAG_MAP_STATE", "onActivityResult");
        if (requestCode == GpsProviderHelper.REQUEST_ENABLE_LOCATION_SERVICE) {
            if (resultCode == RESULT_OK) {
                getLocation();
            } else {
                locationAlertDialog.show();

            }
        }

    }
    private void zoomToPound(List<LatLng> points) {
        if (mGoogleMap == null) return;
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : points) {
            boundsBuilder.include(latLngPoint);
            Log.i(TAG, "zoomToPound: " + latLngPoint.latitude);
        }

        int routePadding = 100;
        LatLngBounds latLngBounds = boundsBuilder.build();

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
    }

    /**
     * add list of marker to map
     *
     * @param latLngList list of point in map
     */
    private void addMarker(List<LatLng> latLngList) {
        if (mGoogleMap == null) return;
        int index = 0;
        for (LatLng latLng : latLngList) {
            mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))).setTag(index);
            index++;
        }
    }

    private void addBranchToMap(List<LatLng> latLngList){
        if (latLngList==null||latLngList.isEmpty())return;

        addMarker(latLngList);
        zoomToPound(latLngList);
    }

    private void startNavigation(String uri){
        if (uri == null || TextUtils.isEmpty(uri)) return;

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(Intent.createChooser(intent, "Select an application"));
    }
    private void showSlide(View view){
        Log.i(TAG, "onClick: " + slideToggle);

        if (slideToggle) {
            mMapBinding.slideMainRelative.animate().translationY(0);
            mMapBinding.sliderImageView.setImageResource(R.drawable.ic_arrow_up);

        } else {
            mMapBinding.slideMainRelative.animate().translationY(mMapBinding.slideUpRelativelayout.getHeight() * -1);
            mMapBinding.sliderImageView.setImageResource(R.drawable.ic_arrow_point_to_down);
        }
        slideToggle = !slideToggle;
    }

    private void showDataOnSlide(Branch branch) {
        if (branch == null) return;

        mMapBinding.slideMainRelative.setVisibility(View.VISIBLE);

        mMapBinding.mabAddressTextview.setText(branch.getAddress());
        mMapBinding.mapCountryTextview.setText(branch.getCountryName(this));
        mMapViewModel.calculateDistance();
    }

    /**
     * check all permission and gps is turn on
     * and called method to get current location
     */
    public void getLocation() {
        if (GpsProviderHelper.hasPermission(this)) {
            if (GpsProviderHelper.isGPSEnabled(this)) {
                Log.i(TAG, "getLocation: ");
                mMapViewModel.getCurrentLocation();
            } else {
                mGpsProvider.enableGps();
            }
        } else {
            mGpsProvider.onNeedPermission();
        }
    }


    private boolean markerClick(Marker marker) {
        if (marker==null||marker.getTag()==null||mMapViewModel.getBranchesMutableLiveData().getValue()==null)return false;

        Branch branchesLstItem = mMapViewModel.getBranchesMutableLiveData().getValue().get((Integer) marker.getTag());
        mMapViewModel.getSelectedBranchMutableLiveData().setValue(branchesLstItem);

        return true;
    }



}
