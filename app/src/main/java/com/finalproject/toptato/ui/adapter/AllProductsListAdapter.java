package com.finalproject.toptato.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.toptato.databinding.RowItemProductBinding;
import com.finalproject.toptato.model.entities.api.Product;

import java.util.List;

/**
 * Created by Osman Ibrahiem
 */
public class AllProductsListAdapter extends RecyclerView.Adapter<AllProductsListAdapter.AllProductsListViewHolder> {

    private OnItemClickListener listener;
    private List<Product> productList;

    public AllProductsListAdapter() {
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AllProductsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowItemProductBinding binding = RowItemProductBinding.inflate(inflater, parent, false);
        return new AllProductsListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AllProductsListViewHolder holder, int position) {
        holder.bindTo(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    public interface OnItemClickListener {
        void onClick(Product product);
    }

    class AllProductsListViewHolder extends RecyclerView.ViewHolder {

        private RowItemProductBinding binding;

        AllProductsListViewHolder(@NonNull RowItemProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindTo(Product product) {
            binding.setProduct(product);
            binding.rootView.setOnClickListener(v -> {
                if (listener != null)
                    listener.onClick(product);
            });
        }
    }
}
