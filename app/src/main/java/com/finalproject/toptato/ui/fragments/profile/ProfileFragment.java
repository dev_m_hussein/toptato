package com.finalproject.toptato.ui.fragments.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.FragmentProfileBinding;
import com.finalproject.toptato.model.entities.profile.ProfileView;
import com.finalproject.toptato.model.viewmodels.profile.ProfileViewModel;
import com.finalproject.toptato.ui.activities.branchesactivity.BranchesActivity;
import com.finalproject.toptato.ui.activities.loginactivity.LoginActivity;
import com.finalproject.toptato.ui.activities.signupactivity.SignUpActivity;

public class ProfileFragment extends Fragment implements ProfileView {


    private FragmentProfileBinding binding;
    private ProfileViewModel profileViewModel;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);


        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        profileViewModel.setProfileView(this);
        binding.setProfileViewModel(profileViewModel);

        /**
         * sorry
         */
        binding.txtBranches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), BranchesActivity.class));
            }
        });

        return binding.getRoot();
    }


    @Override
    public void startSignUp() {

        Intent intent = new Intent(getContext(), SignUpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void startLogIn() {

        Intent intent = new Intent(getContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

}
