package com.finalproject.toptato.ui.activities.signupactivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.ActivitySignUpBinding;
import com.finalproject.toptato.model.utils.AlertDialogUtils;
import com.finalproject.toptato.model.utils.validation.EmailValid;
import com.finalproject.toptato.model.utils.validation.EmptyValid;
import com.finalproject.toptato.model.utils.validation.MethodsValid;
import com.finalproject.toptato.model.utils.validation.PasswordValid;
import com.finalproject.toptato.model.utils.validation.PhoneNumberValid;
import com.finalproject.toptato.model.viewmodels.signup.SignUpViewModel;
import com.finalproject.toptato.ui.activities.loginactivity.LoginActivity;
import com.finalproject.toptato.ui.fragments.PickerFragment.DataPickerFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by mahmoud saad
 */
public class SignUpActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private final String TAG = getClass().getSimpleName();
    private final int EDIT_TEXT_DELAY = 800;
    private ActivitySignUpBinding mSignUpBinding;
    private SignUpViewModel mSignUpViewModel;
    private DataPickerFragment mPickerFragment;
    private AlertDialog alarmAlertDialog, progressBarAlertDialog;

    private Handler handler[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSignUpBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        mSignUpBinding.setLifecycleOwner(this);

        mSignUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);

        mSignUpBinding.setViewModle(mSignUpViewModel);

        initProducts();

        observables();

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);


        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", new Locale("en"));
        String strDate = format.format(calendar.getTime());

        mSignUpViewModel.getDataOfBirthMutableLiveData().setValue(strDate);

    }

    public void initProducts() {

        setSupportActionBar(mSignUpBinding.signUpToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        //prepare the dialog fragment
        alarmAlertDialog = AlertDialogUtils.createAlarmAlertDialog(this, getResources().getString(R.string.alert_dialog_title_error), "");
        progressBarAlertDialog = AlertDialogUtils.createProgressBarAlertDialog(this);

        mPickerFragment = new DataPickerFragment();

        mSignUpBinding.signUpDataOfBirthEdittext.setOnClickListener(v -> mPickerFragment.show(getSupportFragmentManager(), "fragmentPicker")
        );

        handler=new Handler[8];
        for (int i = 0; i < handler.length; i++) {
            handler[i]=new Handler();
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(this, LoginActivity.class));
        }

        return true;
    }

    public void observables() {

        mSignUpViewModel.getEmail().observe(this,s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpEmailEdittext,
                        new EmailValid(),
                        s,
                        handler[0],
                        getResources().getString(R.string.message_email_error),
                        EDIT_TEXT_DELAY)
        );


        mSignUpViewModel.getPassword().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpPasswordEdittext,
                        new PasswordValid(),
                        s,
                        handler[1],
                        getResources().getString(R.string.message_password_error),
                        EDIT_TEXT_DELAY)
        );
        mSignUpViewModel.getFirstName().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpFirstNameEdittext,
                        new EmptyValid(),
                        s,
                        handler[2],
                        getResources().getString(R.string.message_not_empty_error),
                        EDIT_TEXT_DELAY)
        );
        mSignUpViewModel.getLastName().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpLastNameEdittext,
                        new EmptyValid(),
                        s,
                        handler[3],
                        getResources().getString(R.string.message_not_empty_error),
                        EDIT_TEXT_DELAY)
        );
        mSignUpViewModel.getPhoneNumber().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpPhoneNumberEdittext,
                        new PhoneNumberValid(),
                        s,
                        handler[4],
                        getResources().getString(R.string.message_phone_error),
                        EDIT_TEXT_DELAY)
        );

        mSignUpViewModel.getAddress().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpAddressEdittext,
                        new EmptyValid(),
                        s,
                        handler[5],
                        getResources().getString(R.string.message_not_empty_error),
                        EDIT_TEXT_DELAY)
        );
        mSignUpViewModel.getStreet().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpStreetEdittext,
                        new EmptyValid(),
                        s,
                        handler[6],
                        getResources().getString(R.string.message_not_empty_error),
                        EDIT_TEXT_DELAY)
        );

        mSignUpViewModel.getDataOfBirthMutableLiveData().observe(this, s -> MethodsValid.inputValid(
                        mSignUpBinding.signUpDataOfBirthEdittext,
                        new EmptyValid(),
                        s,
                        handler[7],
                        getResources().getString(R.string.message_birhday_error),
                        EDIT_TEXT_DELAY)
        );

        mSignUpViewModel.getDisplayMessage().observe(this,this::showMessage);

        mSignUpViewModel.getGenderFemale().observe(this, aBoolean -> mSignUpViewModel.getGenderError().setValue(false));

        mSignUpViewModel.getGenderMale().observe(this, aBoolean -> mSignUpViewModel.getGenderError().setValue(false));

        mSignUpViewModel.getStartNewActivity().observe(this,this::startLoginActivity);

        mSignUpViewModel.isLoading().observe(this,this::showPrgressDialog);

        mSignUpViewModel.getGenderError().observe(this, aBoolean -> {
            if (aBoolean) {
                mSignUpBinding.femaleRadioButton.setError(getResources().getString(R.string.message_gender_error));
            } else {
                mSignUpBinding.femaleRadioButton.setError(null);
                mSignUpBinding.maleRadioButton.setError(null);
            }
        });

    }

    private void showPrgressDialog(Boolean aBoolean) {
        if (aBoolean) {
            progressBarAlertDialog.show();
        } else {
            progressBarAlertDialog.dismiss();
        }
    }

    private void startLoginActivity(Boolean aBoolean) {
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        intent.putExtra(getResources().getString(R.string.login_activation_code_key), aBoolean);
        startActivity(intent);
    }

    private void showMessage(String s) {
        alarmAlertDialog.setMessage(s);
        alarmAlertDialog.show();
    }


}
