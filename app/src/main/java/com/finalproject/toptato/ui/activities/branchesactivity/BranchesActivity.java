package com.finalproject.toptato.ui.activities.branchesactivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.ActivityBranchesBinding;
import com.finalproject.toptato.model.entities.api.Branch;
import com.finalproject.toptato.model.interfaces.RecyclerItemClick;
import com.finalproject.toptato.model.utils.AlertDialogUtils;
import com.finalproject.toptato.model.utils.GpsProviderHelper;
import com.finalproject.toptato.model.viewmodels.branches.BranchesViewModel;
import com.finalproject.toptato.ui.activities.mapActivity.MapActivity;
import com.finalproject.toptato.ui.adapter.BranchesListAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahmoud saad
 */

public class BranchesActivity extends AppCompatActivity implements RecyclerItemClick {
    private final String TAG = getClass().getSimpleName();
    AlertDialog alertDialog, progresBarAlertDialog;
    private ActivityBranchesBinding mBranchesBinding;
    private BranchesViewModel mBranchesViewModel;
    private BranchesListAdapter mBranchAdapter;
    private List<Branch> lastBranchesLstItems;
    private AlertDialog locationAlertDialog;
    /**
     * list of country that have branches
     */
    private String[] countryNamesArray;
    private List<Integer> idCountryList;
    /**
     * listener for click on alert dialog item
     * then get list of country and send to recyclerView
     */
    DialogInterface.OnClickListener alertDialogLisener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {


            //check if id of country is null or is empty
            if (idCountryList != null && !idCountryList.isEmpty())

                //get list of branches
                lastBranchesLstItems = mBranchesViewModel.getBranchesMapMutableLiveData().getValue().get(idCountryList.get(which));

            if (lastBranchesLstItems != null && !lastBranchesLstItems.isEmpty()) {

                //clear old data from recycler
                mBranchAdapter.clearItemList();

                //add new data to recycler
                mBranchAdapter.setmItemList(lastBranchesLstItems);

                //change text the user select instead of country old selected or text " select your country "
                mBranchesBinding.nameBranchTextView.setText(countryNamesArray[which]);
            }
        }
    };
    private GpsProviderHelper mGpsProvider = new GpsProviderHelper() {
        @Override
        public Activity getActivity() {
            return BranchesActivity.this;
        }

        @Override
        public void onNeedPermission() {
            super.onNeedPermission();
            ActivityCompat.requestPermissions(BranchesActivity.this
                    , new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION
                            , Manifest.permission.ACCESS_COARSE_LOCATION}
                    , PERMISSION_LOCATION_REQUEST_CODE
            );
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBranchesBinding = DataBindingUtil.setContentView(this, R.layout.activity_branches);
        mBranchesBinding.setLifecycleOwner(this);


        mBranchesViewModel = ViewModelProviders.of(this).get(BranchesViewModel.class);


        //when click on layout select your country
        mBranchesBinding.branchRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) alertDialog.show();
            }
        });

        //when click map image
        mBranchesBinding.mapRelative.setOnClickListener(v -> {
            if (lastBranchesLstItems != null) {
                Intent intent = new Intent(BranchesActivity.this, MapActivity.class);
                if (mBranchesViewModel.getCurrentLocationMutableLiveData().getValue() != null) {
                    Location location = mBranchesViewModel.getCurrentLocationMutableLiveData().getValue();
                    intent.putExtra(getResources().getString(R.string.location_latitude_key), location.getLatitude());
                    intent.putExtra(getResources().getString(R.string.location_longitude_key), location.getLongitude());
                }

                intent.putExtra(getResources().getString(R.string.branches_selected_key), (Serializable) lastBranchesLstItems);
                BranchesActivity.this.startActivity(intent);
            }
        });
        initActivity();


    }

    void initActivity() {

        //get all branches from server
        mBranchesViewModel.getBranches();

        setUpRecycler();

        observables();

        initAlertDialog();

        getLocation();


    }

    void initAlertDialog() {
        locationAlertDialog = AlertDialogUtils.createAlarmAlertDialog(
                BranchesActivity.this,
                getResources().getString(R.string.alert_dialog_title_warning),
                getResources().getString(R.string.message_warning_open_gps)
        );
        progresBarAlertDialog = AlertDialogUtils.createProgressBarAlertDialog(this);

    }

    void setUpRecycler() {
        mBranchAdapter = new BranchesListAdapter(this, mBranchesViewModel.getCurrentLocationMutableLiveData());

        mBranchesBinding.branchesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mBranchesBinding.branchesRecyclerView.setAdapter(mBranchAdapter);

    }

    /**
     * check all permission and gps is turn on
     * and last called method to get current location
     */
    public void getLocation() {
        if (GpsProviderHelper.hasPermission(this)) {
            if (GpsProviderHelper.isGPSEnabled(this)) {
                Log.i(TAG, "getLocation: ");
                mBranchesViewModel.getCurrentLocation();
            } else {
                mGpsProvider.enableGps();
            }
        } else {
            mGpsProvider.onNeedPermission();
        }
    }

    void observables() {
        mBranchesViewModel.getBranchesMapMutableLiveData().observe(this, integerListMap -> {
            //inti list of id country
            idCountryList = new ArrayList<>(integerListMap.keySet());

            //add all id from map

            //init array of county names to show in alert dialog
            countryNamesArray = mBranchesViewModel.getCountryName(integerListMap);

            if (countryNamesArray == null) return;

            Log.i(TAG, "onChanged: is callled");


            //create alert dialog
            alertDialog = AlertDialogUtils.createListAlertDialog(
                    BranchesActivity.this,
                    getResources().getString(R.string.branches_select_your_country),
                    countryNamesArray,
                    alertDialogLisener
            );
        });

        mBranchesViewModel.isLoading().observe(this, aBoolean -> {
            if (aBoolean) progresBarAlertDialog.show();
            else progresBarAlertDialog.dismiss();
        });
    }

    /**
     * when click on item of recycler get the branch of user selected and sent it to MapActivity
     *
     * @param branchesLstItem
     */
    @Override
    public void onClick(Branch branchesLstItem) {
        Toast.makeText(this, "main", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(BranchesActivity.this, MapActivity.class);
        if (mBranchesViewModel.getCurrentLocationMutableLiveData().getValue() != null) {
            Location location = mBranchesViewModel.getCurrentLocationMutableLiveData().getValue();
            intent.putExtra(getResources().getString(R.string.location_latitude_key), location.getLatitude());
            intent.putExtra(getResources().getString(R.string.location_longitude_key), location.getLongitude());
        }
        intent.putExtra(getResources().getString(R.string.branch_selected_key), branchesLstItem);
        BranchesActivity.this.startActivity(intent);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GpsProviderHelper.PERMISSION_LOCATION_REQUEST_CODE) {
            if (!GpsProviderHelper.hasPermission(getApplicationContext())) {
                locationAlertDialog.show();
            } else {
                getLocation();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("TAG_MAP_STATE", "onActivityResult");
        switch (requestCode) {
            case GpsProviderHelper.REQUEST_ENABLE_LOCATION_SERVICE:
                if (resultCode == RESULT_OK) {
                    getLocation();
                } else {
                    locationAlertDialog.show();

                }

                break;
        }

    }

}
