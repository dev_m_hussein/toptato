package com.finalproject.toptato.ui.fragments.favourite;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.finalproject.toptato.R;
import com.finalproject.toptato.databinding.FragmentFavouriteBinding;
import com.finalproject.toptato.model.entities.api.Product;
import com.finalproject.toptato.model.utils.AlertDialogUtils;
import com.finalproject.toptato.model.viewmodels.favourite.FavouriteViewModel;
import com.finalproject.toptato.ui.adapter.AllFavoritesListAdapter;

import java.util.List;

public class FavouriteFragment extends Fragment {

    private FavouriteViewModel favouriteViewModel;
    private FragmentFavouriteBinding binding;
    private AllFavoritesListAdapter adapter;
    private AlertDialog dialogUtils;
    private ProgressDialog dialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        favouriteViewModel =
                ViewModelProviders.of(this).get(FavouriteViewModel.class);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourite, container, false);
        binding.setLifecycleOwner(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        binding.favRecyclerView.setLayoutManager(gridLayoutManager);

        dialog=new ProgressDialog(getActivity());

        Observales();

        return binding.getRoot();
    }

    private void Observales() {

       adapter =new AllFavoritesListAdapter();

        dialogUtils=AlertDialogUtils.createAlarmAlertDialog(getActivity() , "Warning" , "You Should LogIn First" );

        binding.favRecyclerView.setAdapter(adapter);

        favouriteViewModel.getFavoritesListItems();

        favouriteViewModel.isLogin.observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {

                if (aBoolean){

                    LoadFavoritesFromViewModel();

                    if (dialogUtils!=null){

                        dialogUtils.dismiss();
                    }

                }else {
                 dialogUtils.show();
                }
            }
        });



        favouriteViewModel.isLoading().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {

                showLoading(aBoolean);
            }
        });
    }

    private void LoadFavoritesFromViewModel() {
        favouriteViewModel.getfavoriteListItemMutable.observe(getActivity(), new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                adapter.setAllFavoriteList(products);

            }
        });
    }

    private void showLoading(boolean isLoading) {
        if (isLoading) {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } else {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

}
